package geometry

import (
	. "gopkg.in/check.v1"
)

type PolyhedronSuite struct{}

var _ = Suite(&PolyhedronSuite{})

func (s PolyhedronSuite) TestNew(c *C) {
	poly, err := NewPolyhedron(Triangles{
		Triangle{
			NewPoint(0, 0, 0),
			NewPoint(0, 1, 0),
			NewPoint(1, 0, 0),
		},
		Triangle{
			NewPoint(0, 1, 0),
			NewPoint(1, 0, 0),
			NewPoint(1, 1, 0),
		},
		Triangle{
			NewPoint(1, 0, 0),
			NewPoint(1, 1, 0),
			NewPoint(0, 0, 0),
		},
		Triangle{
			NewPoint(0, 0, 0),
			NewPoint(1, 1, 0),
			NewPoint(0, 1, 0),
		},
	})
	c.Assert(err, IsNil)
	c.Assert(poly, HasLen, 4)

	c.Check(poly.ContainsPoint(NewPoint(0.5, 0.5, 0)), Equals, true)
	c.Check(poly.ContainsPoint(NewPoint(2, 0, 0)), Equals, false)
	c.Check(poly.ContainsPoint(poly.Points().Center()), Equals, true)
}

func (s PolyhedronSuite) TestNewPolyhedronFromCylinder(c *C) {
	pt := Point{0, 0, 0}
	_, err := NewPolyhedronFromCylinder(pt, 20, 1000)
	c.Assert(err, IsNil)
}
