package geometry

import (
	"fmt"

	mapset "github.com/deckarep/golang-set"
	"gitlab.com/skyy.network/skyy-blockchain/units"
)

func Dimensional(locs units.Locations, padding units.Measurement, fType Fill, limit int) (units.Locations, error) {
	if len(locs) > 2 {
		return ThreeDimensional(locs, limit), nil
	}
	switch len(locs) {
	case 1:
		return OneDimensional(locs[0], padding, fType, limit), nil
	case 2:
		return TwoDimensional(
			locs[0], locs[1], padding, fType, limit,
		), nil
	default:
		return nil, fmt.Errorf("Unsupport number of locations: %d", len(locs))
	}
}

// convert a set of location strings to a list of locations
func setToLocations(set mapset.Set) units.Locations {
	// build locales slice
	locs := units.Locations{}
	it := set.Iterator()
	for locStr := range it.C {
		loc, _ := units.ParseLocation(locStr.(string))
		locs = append(locs, loc)
	}
	return locs
}
