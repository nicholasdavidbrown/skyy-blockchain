package geometry

import (
	. "gopkg.in/check.v1"
)

type BaseSuite struct{}

var _ = Suite(&BaseSuite{})

func (s *BaseSuite) TestCenter(c *C) {
	p1 := NewPoint(0, 0, 0)
	p2 := NewPoint(1, 1, 1)
	center := Points{p1, p2}.Center()
	c.Check(center.X, Equals, 0.5)
	c.Check(center.Y, Equals, 0.5)
	c.Check(center.Z, Equals, 0.5)
}

func (s *BaseSuite) TestDistance(c *C) {
	a := NewPoint(0, 0, 0)
	b := NewPoint(10, 10, 0)
	c.Check(a.Distance(b), Equals, 14.142135623730951)

	a = NewPoint(0, 0, 0)
	b = NewPoint(10, 10, 10)
	c.Check(a.Distance(b), Equals, 17.320508075688775)
}

func (s *BaseSuite) TestTriangeSegmentIntersection(c *C) {
	// To visualize this lines/triangles, use http://geogebra.org/3d
	// Do intersect
	line := Line{
		NewPoint(0, 0, 0),
		NewPoint(0, 0, 10),
	}
	p := Triangle{
		NewPoint(-5, -5, 5),
		NewPoint(5, 0, 5),
		NewPoint(-5, 5, 5),
	}
	intersect := p.LineIntersects(line)
	c.Assert(intersect, Equals, true)
	line = Line{
		NewPoint(0, 0, 0),
		NewPoint(-8, 8, 8),
	}
	p = Triangle{
		NewPoint(10, 10, 5),
		NewPoint(-10, -10, 5),
		NewPoint(-10, 10, 5),
	}
	intersect = p.LineIntersects(line)
	c.Assert(intersect, Equals, true)
	line = Line{
		NewPoint(0, 0, 0),
		NewPoint(0, 0, 10),
	}
	p = Triangle{
		NewPoint(-5, 5, 5),
		NewPoint(5, 5, 5),
		NewPoint(-5, -5, 5),
	}
	intersect = p.LineIntersects(line)
	c.Check(intersect, Equals, true)

	// do not intersect
	line = Line{
		NewPoint(0, 0, 0),
		NewPoint(0, 0, 10),
	}
	p = Triangle{
		NewPoint(2, 1, 5),
		NewPoint(2, 2, 5),
		NewPoint(10, 5, 5),
	}
	intersect = p.LineIntersects(line)
	c.Assert(intersect, Equals, false)
	line = Line{
		NewPoint(0, 0, 0),
		NewPoint(0, 0, 10),
	}
	p = Triangle{
		NewPoint(-1, -1, 20),
		NewPoint(1, -1, 20),
		NewPoint(1, 1, 20),
	}
	intersect = p.LineIntersects(line)
	c.Check(intersect, Equals, false)
	line = Line{
		NewPoint(0, 0, 0),
		NewPoint(0, 0, 10),
	}
	p = Triangle{
		NewPoint(0, 0, 5),
		NewPoint(0, 10, 10),
		NewPoint(0, 10, 5),
	}
	intersect = p.LineIntersects(line) // line is parallel to poly
	c.Check(intersect, Equals, false)
	line = Line{
		NewPoint(-6, 0, 0),
		NewPoint(6, 0, 0),
	}
	p = Triangle{
		NewPoint(5, -5, 0.1),
		NewPoint(5, 5, 0.1),
		NewPoint(-5, 5, 0.1),
	}
	intersect = p.LineIntersects(line)
	c.Check(intersect, Equals, false)
}

func (s *BaseSuite) TestFindAngle(c *C) {
	x, y, z := findAngles(3, 3, 3)
	c.Check(x, Equals, 60.0)
	c.Check(y, Equals, 60.0)
	c.Check(z, Equals, 60.0)

	x, y, z = findAngles(8, 7, 6)
	c.Check(x, Equals, 75.522488)
	c.Check(y, Equals, 57.910049000000015)
	c.Check(z, Equals, 46.567463)

	x, y, z = findAngles(8000000, 7000000, 6000000)
	c.Check(x, Equals, 75.522488)
	c.Check(y, Equals, 57.910049000000015)
	c.Check(z, Equals, 46.567463)
}

func (s *BaseSuite) TestConvert(c *C) {
	c.Check(degreesToRadians(90), Equals, 1.5707963267948966)
	c.Check(radiansToDegrees(1.5707963267948966), Equals, 90.0)
}
