package geometry

import (
	"gitlab.com/skyy.network/skyy-blockchain/units"
	. "gopkg.in/check.v1"
)

type TwoDimensionalSuite struct{}

var _ = Suite(&TwoDimensionalSuite{})

func (s *TwoDimensionalSuite) TestGetIndex(c *C) {
	c.Check(getIndex(0, 5, 5), Equals, 0)
	c.Check(getIndex(1, 5, 5), Equals, 1)
	c.Check(getIndex(2, 5, 5), Equals, 2)
	c.Check(getIndex(3, 5, 5), Equals, 3)
	c.Check(getIndex(4, 5, 5), Equals, 4)

	c.Check(getIndex(0, 5, 6), Equals, 0)
	c.Check(getIndex(1, 5, 6), Equals, 1)
	c.Check(getIndex(2, 5, 6), Equals, 2)
	c.Check(getIndex(3, 5, 6), Equals, 2)
	c.Check(getIndex(4, 5, 6), Equals, 3)
	c.Check(getIndex(5, 5, 6), Equals, 4)

	c.Check(getIndex(0, 5, 10), Equals, 0)
	c.Check(getIndex(1, 5, 10), Equals, 0)
	c.Check(getIndex(2, 5, 10), Equals, 1)
	c.Check(getIndex(3, 5, 10), Equals, 1)
	c.Check(getIndex(4, 5, 10), Equals, 2)
	c.Check(getIndex(5, 5, 10), Equals, 2)
	c.Check(getIndex(6, 5, 10), Equals, 3)
	c.Check(getIndex(7, 5, 10), Equals, 3)
	c.Check(getIndex(8, 5, 10), Equals, 4)
	c.Check(getIndex(9, 5, 10), Equals, 4)

	c.Check(getIndex(0, 3, 8), Equals, 0)
	c.Check(getIndex(1, 3, 8), Equals, 0)
	c.Check(getIndex(2, 3, 8), Equals, 1)
	c.Check(getIndex(3, 3, 8), Equals, 1)
	c.Check(getIndex(4, 3, 8), Equals, 1)
	c.Check(getIndex(5, 3, 8), Equals, 2)
	c.Check(getIndex(6, 3, 8), Equals, 2)
	c.Check(getIndex(7, 3, 8), Equals, 2)

	c.Check(getIndex(0, 5, 8), Equals, 0)
	c.Check(getIndex(1, 5, 8), Equals, 1)
	c.Check(getIndex(2, 5, 8), Equals, 1)
	c.Check(getIndex(3, 5, 8), Equals, 2)
	c.Check(getIndex(4, 5, 8), Equals, 2)
	c.Check(getIndex(5, 5, 8), Equals, 3)
	c.Check(getIndex(6, 5, 8), Equals, 3)
	c.Check(getIndex(7, 5, 8), Equals, 4)
}

func (s *TwoDimensionalSuite) TestModNum(c *C) {
	c.Check(modNum(0, 10), Equals, 8)
	c.Check(modNum(1, 10), Equals, 6)
	c.Check(modNum(2, 10), Equals, 4)
	c.Check(modNum(3, 10), Equals, 2)
	c.Check(modNum(4, 10), Equals, 0)
	c.Check(modNum(5, 10), Equals, 1)
	c.Check(modNum(6, 10), Equals, 3)
	c.Check(modNum(7, 10), Equals, 5)
	c.Check(modNum(8, 10), Equals, 7)
	c.Check(modNum(9, 10), Equals, 9)
}

func (s *TwoDimensionalSuite) TestGet2D(c *C) {
	loc1 := units.NewLocation(
		units.Latitude(3.40240),
		units.Longitude(-112.49897),
		units.NewAltitude(
			units.NewMeasurement(1000, units.Meters),
			units.AMSL,
		),
	)

	loc2 := units.NewLocation(
		units.Latitude(3.40255),
		units.Longitude(-112.49867),
		units.NewAltitude(
			units.NewMeasurement(1050, units.Meters),
			units.AMSL,
		),
	)
	padding := units.NewMeasurement(0, units.Meters)
	locales := TwoDimensional(loc1, loc2, padding, Circle, 1000)

	/*
		sort.Slice(locales, func(i, j int) bool {
			return locales[i].Altitude.Measurement.Amount < locales[j].Altitude.Measurement.Amount
		})
		for _, loc := range locales {
			log.Printf("LOC: %s", loc.String())
		}
	*/

	c.Check(locales, HasLen, 6, Commentf("Len: %d", len(locales)))

}
