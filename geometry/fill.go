package geometry

import (
	"math"
	"strings"

	mapset "github.com/deckarep/golang-set"

	"gitlab.com/skyy.network/skyy-blockchain/units"
)

type Fill string

var (
	Circle  Fill = "circle"
	Diamond Fill = "diamond"
	Box     Fill = "box"
)

func StringToFill(s string) Fill {
	switch strings.ToLower(s) {
	case "circle":
		return Circle
	case "diamond":
		return Diamond
	case "box", "cube", "rectangle":
		return Box
	default:
		return Circle
	}
}

func fill(set mapset.Set, fillType Fill, points units.Locations, limit int) {
	lats := make(units.Latitudes, len(points))
	longs := make(units.Longitudes, len(points))
	alts := make(units.Altitudes, len(points))

	for i, loc := range points {
		lats[i] = loc.Latitude
		longs[i] = loc.Longitude
		alts[i] = loc.Altitude
	}

	latRange := units.RangeLatitudes(lats)
	longRange := units.RangeLongitudes(longs)
	altRange := units.RangeAltitudes(alts)
	// The maximum distance a point can be from the original point (loc)
	var max float64
	switch fillType {
	case Circle:
		max = math.Pow(float64(maxPoints(len(altRange))), 2)
	case Diamond:
		max = float64(maxPoints(len(altRange)))
	case Box:
		max = 1.0 // max doesn't matter since we're adding all points
	default:
		panic("Unsupported Fill type")
	}

	// TODO: optimize performance here
	var count int
	for i, alt := range altRange {
		altPoint := pointer(i, len(altRange))
		for j, long := range longRange {
			longPoint := pointer(j, len(longRange))
			for k, lat := range latRange {
				latPoint := pointer(k, len(latRange))
				if pointCalc(fillType, altPoint, latPoint, longPoint) > max {
					// points are too high, outside our circle around our one
					// dimensional object
					continue
				}
				loc := units.NewLocation(lat, long, alt)
				set.Add(loc.String())
				count += 1
				if count > limit {
					set.Clear()
				}
			}
		}
	}
}

// Calculate the distance from center these coordinates are
func pointCalc(fillType Fill, i, j, k int) float64 {
	switch fillType {
	case Circle:
		return (math.Pow(float64(i), 2) + math.Pow(float64(j), 2) + math.Pow(float64(k), 2))
	case Diamond:
		return float64(i + j + k)
	case Box:
		// force the number to always be small so we add all points in our box
		return 0
	default:
		panic("Unsupported Fill type")
	}
}

// calculate the number of "hops" from center we had to take
func pointer(i, count int) int {
	maxPts := maxPoints(count)
	return int(math.Abs(float64(maxPts - (i))))
}

// Find the maximum number of "hops" we can take from center
func maxPoints(count int) int {
	return int(math.Floor(float64(count) / 2.0))
}
