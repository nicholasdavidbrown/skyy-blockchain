package geometry

import (
	"fmt"
	"math"
	"sort"
	"strings"
)

type Polyhedron Triangles
type Polyhedrons []Polyhedron

// Create a Polyhedron instance
func NewPolyhedron(triangles Triangles) (Polyhedron, error) {
	if len(triangles) < 3 {
		return Polyhedron{}, fmt.Errorf("Not enough triangles (%d) to make a polyhedron", len(triangles))
	}

	for _, triangle := range triangles {
		if !triangle.IsValid() {
			return Polyhedron{}, fmt.Errorf(
				"Invalid Triangle: %s", triangle.String(),
			)
		}
	}

	poly := Polyhedron(triangles)
	if !poly.IsConvex() {
		return Polyhedron{}, fmt.Errorf(
			"Only convex polyhedrons are currently supported",
		)
	}

	if !poly.IsClosed() {
		return Polyhedron{}, fmt.Errorf(
			"The given Polyhedron has an open shape, which is not allowed",
		)
	}

	return poly, nil
}

func NewPolyhedronFromCylinder(point Point, radius, height float64) (Polyhedron, error) {
	numSides := 16                      // number of "pieces" to our pie
	angleA := 360.0 / float64(numSides) // inner angle of each "pie" in our circle

	findSides := func(ang, radius float64) (float64, float64) {
		angA := 90.0 - ang
		angB := 90.0 - angA
		sidA := radius
		sidB := sidA * math.Sin(degreesToRadians(angA)) / math.Sin(degreesToRadians(90))
		sidC := sidA * math.Sin(degreesToRadians(angB)) / math.Sin(degreesToRadians(90))
		return sidC, sidB
	}

	// Build list of points/vertices
	vertices := make(Points, numSides)
	for i := range vertices {
		a, b := findSides(angleA*float64(i%(numSides/4)), radius)
		pt := point
		switch i / 4 {
		case 0:
			pt.X += a
			pt.Y += b
		case 1:
			pt.X += b
			pt.Y -= a
		case 2:
			pt.X -= a
			pt.Y -= b
		case 3:
			pt.X -= b
			pt.Y += a
		}
		vertices[i] = pt
	}

	// build triangles
	triangles := make(Triangles, 0)
	// creates the walls of the cylinder
	for i, pt := range vertices {
		var pt2 Point
		if (i + 1) < len(vertices) {
			pt2 = vertices[i+1]
		} else {
			pt2 = vertices[0]
		}

		hpt := pt
		hpt.Z = height
		tri := NewTriangle(pt, hpt, pt2)
		triangles = append(triangles, tri)
		pt3 := pt2
		pt3.Z = height
		tri = NewTriangle(hpt, pt2, pt3)
		triangles = append(triangles, tri)
	}

	// adds floor/ceiling of cylinder
	for i, pt := range vertices {
		var pt2 Point
		if (i + 1) < len(vertices) {
			pt2 = vertices[i+1]
		} else {
			pt2 = vertices[0]
		}

		pt3 := point
		tri := NewTriangle(pt, pt2, pt3)
		triangles = append(triangles, tri)

		pt.Z = height
		pt2.Z = height
		pt3.Z = height
		tri = NewTriangle(pt, pt2, pt3)
		triangles = append(triangles, tri)
	}

	return NewPolyhedron(triangles)
}

// Check if all triangles within a polyhedron created a closed shape
// In order for a polydron to be closed, each set of two points in a triangle
// are referenced twice.
func (p Polyhedron) IsClosed() bool {
	m := make(map[string]int)

	for _, triangle := range p {
		for _, line := range triangle.Lines() {
			pts := []string{line[0].String(), line[1].String()}
			sort.Strings(pts) // sorting so order DOES NOT matter
			m[strings.Join(pts, " ")] += 1
		}
	}

	var failCount int
	for k, v := range m {
		if v != 2 {
			fmt.Printf("Close fail: %s (%d)\n", k, v)
			failCount += 1
			//return false
		}
	}
	if failCount > 0 {
		fmt.Printf("Fail Count: %d\n", failCount)
		return false
	}

	return true
}

// TODO: check if polyhedron is convex
func (p Polyhedron) IsConvex() bool {
	return true
}

func (p Polyhedron) ContainsPoint(pt Point) bool {
	points := p.Points()

	var targetTotal float64
	for _, point := range points {
		targetTotal += points[0].Distance(point)
	}

	var pointTotal float64
	for _, point := range points {
		pointTotal += point.Distance(pt)
	}
	if pointTotal > targetTotal {
		return false
	}

	return true
}

func (p Polyhedron) Points() (points Points) {
	for _, triangle := range p {
		points = append(points, triangle.Points()...)
	}
	return
}
