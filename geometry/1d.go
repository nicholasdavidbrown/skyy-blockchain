package geometry

import (
	mapset "github.com/deckarep/golang-set"

	"gitlab.com/skyy.network/skyy-blockchain/units"
)

// Get the airspace blocks from a one dimensional shape.
// Define the amount of padding around this point in space to include, and the
// type of fill to use.
func OneDimensional(loc units.Location, padding units.Measurement, fillType Fill, limit int) units.Locations {
	set := mapset.NewSet()
	oneDimensional(set, loc, padding, fillType, limit)
	return setToLocations(set)
}

func oneDimensional(set mapset.Set, loc units.Location, padding units.Measurement, fillType Fill, limit int) {
	loc = loc.Snap()
	altMM := loc.Altitude.Measurement

	// Get that radio of 1 padding unit to long/lat at this altitude
	mm := units.NewMeasurement(1, padding.Unit)
	longRatio := loc.Longitude.ConverstionRatio(mm, loc.Altitude)
	latRatio := loc.Latitude.ConverstionRatio(mm, loc.Altitude)

	// create copies of our six points around our center location (loc)
	// center +/- padding on each axis (long/lat/alt)
	altUp := loc
	altUp.Altitude.Measurement = altMM.Add(padding)
	altDown := loc
	altDown.Altitude.Measurement = altMM.Subtract(padding)
	latUp := loc
	latUp.Latitude = units.Latitude(latUp.Latitude.Float64() + (latRatio * padding.Amount))
	latDown := loc
	latDown.Latitude = units.Latitude(latDown.Latitude.Float64() - (latRatio * padding.Amount))
	longUp := loc
	longUp.Longitude = units.Longitude(longUp.Longitude.Float64() + (longRatio * padding.Amount))
	longDown := loc
	longDown.Longitude = units.Longitude(longDown.Longitude.Float64() - (longRatio * padding.Amount))

	// get locations from filling in four points, creating three triangles of fills
	fill(set, fillType, units.Locations{altUp, altDown, longUp, longDown, latUp, latDown}, limit)
}
