package geometry

import (
	. "gopkg.in/check.v1"

	"gitlab.com/skyy.network/skyy-blockchain/units"
)

type OneDimensionalSuite struct{}

var _ = Suite(&OneDimensionalSuite{})

func (s *OneDimensionalSuite) TestOneDimensional(c *C) {
	locale := units.NewLocation(
		units.Latitude(45.6),
		units.Longitude(23),
		units.NewAltitude(
			units.NewMeasurement(1000, units.Meters),
			units.AMSL,
		),
	)
	mm := units.NewMeasurement(30, units.Meters)
	locales := OneDimensional(locale, mm, Circle, 1000)

	/*
		sort.Slice(locales, func(i, j int) bool {
			return locales[i].Altitude.Measurement.Amount < locales[j].Altitude.Measurement.Amount
		})

		for _, loc := range locales {
			log.Printf("LOC: %s", loc.String())
		}
	*/

	c.Assert(locales, HasLen, 123, Commentf("Len: %d", len(locales)))

	locales = OneDimensional(locale, mm, Diamond, 1000)
	c.Assert(locales, HasLen, 63, Commentf("Len: %d", len(locales)))
}
