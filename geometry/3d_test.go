package geometry

import (
	"gitlab.com/skyy.network/skyy-blockchain/units"
	. "gopkg.in/check.v1"
)

type ThreeDimensionalSuite struct{}

var _ = Suite(&ThreeDimensionalSuite{})

func (s *ThreeDimensionalSuite) TestGet3D(c *C) {
	loc1 := units.NewLocation(
		units.Latitude(3.40240),
		units.Longitude(-112.49897),
		units.NewAltitude(
			units.NewMeasurement(1000, units.Meters),
			units.AMSL,
		),
	)

	loc2 := units.NewLocation(
		units.Latitude(3.40255),
		units.Longitude(-112.49867),
		units.NewAltitude(
			units.NewMeasurement(1050, units.Meters),
			units.AMSL,
		),
	)

	loc3 := units.NewLocation(
		units.Latitude(3.40295),
		units.Longitude(-112.49907),
		units.NewAltitude(
			units.NewMeasurement(1070, units.Meters),
			units.AMSL,
		),
	)

	loc4 := units.NewLocation(
		units.Latitude(3.40395),
		units.Longitude(-112.5),
		units.NewAltitude(
			units.NewMeasurement(1095, units.Meters),
			units.AMSL,
		),
	)

	locales := ThreeDimensional(
		units.Locations{loc1, loc2, loc3, loc4}, 3000,
	)

	/*
		sort.Slice(locales, func(i, j int) bool {
			return locales[i].Altitude.Measurement.Amount < locales[j].Altitude.Measurement.Amount
		})
		for _, loc := range locales {
			log.Printf("LOC: %s", loc.String())
		}
	*/

	c.Check(locales, HasLen, 2823, Commentf("Len: %d", len(locales)))

	// check that if we exceed the limit, we return empty
	locales = ThreeDimensional(
		units.Locations{loc1, loc2, loc3, loc4}, 1000,
	)
	c.Check(locales, HasLen, 0, Commentf("Len: %d", len(locales)))
}
