package geometry

import (
	"gitlab.com/skyy.network/skyy-blockchain/units"
)

func ThreeDimensional(locs units.Locations, limit int) units.Locations {
	lats := make(units.Latitudes, len(locs))
	lngs := make(units.Longitudes, len(locs))
	alts := make(units.Altitudes, len(locs))
	for i, loc := range locs {
		lats[i] = loc.Latitude
		lngs[i] = loc.Longitude
		alts[i] = loc.Altitude
	}
	latRange := units.RangeLatitudes(lats)
	lngRange := units.RangeLongitudes(lngs)
	altRange := units.RangeAltitudes(alts)

	center := locs.Center()
	var sum float64
	for _, loc := range locs {
		sum += center.Distance(loc).Amount
	}

	var locales units.Locations
	for _, lat := range latRange {
		for _, lng := range lngRange {
			for _, alt := range altRange {
				tmpLoc := units.NewLocation(lat, lng, alt)
				var tmpSum float64
				for _, loc := range locs {
					tmpSum += tmpLoc.Distance(loc).Amount
				}
				if sum <= tmpSum {
					locales = append(locales, tmpLoc)
				}
				// respect limit
				if len(locales) > limit {
					return nil
				}
			}
		}
	}
	return locales
}
