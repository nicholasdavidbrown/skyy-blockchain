package geometry

import (
	"fmt"
	"math"
	"strconv"
)

const (
	Radian = 1
	Degree = (math.Pi / 180) * Radian
)

type Point struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
	Z float64 `json:"z"`
}
type Points []Point

type Line [2]Point
type Lines []Line
type Triangle [3]Point
type Triangles []Triangle

func NewPoint(x, y, z float64) Point {
	return Point{X: x, Y: y, Z: z}
}

func (p1 Point) Equals(p2 Point) bool {
	return p1.X == p2.X && p1.Y == p2.Y && p1.Z == p2.Z
}

// Find the distance between two points
func (p1 Point) Distance(p2 Point) float64 {
	x := p1.X - p2.X
	y := p1.Y - p2.Y
	z := p1.Z - p2.Z

	return math.Sqrt((x * x) + (y * y) + (z * z))
}

func (p Point) String() string {
	return fmt.Sprintf("%g,%g,%g", p.X, p.Y, p.Z)
}

// Get the center of points
func (points Points) Center() Point {
	xVals := make([]float64, len(points))
	yVals := make([]float64, len(points))
	zVals := make([]float64, len(points))
	for i, point := range points {
		xVals[i] = point.X
		yVals[i] = point.Y
		zVals[i] = point.Z
	}

	return NewPoint(
		average(xVals...),
		average(yVals...),
		average(zVals...),
	)
}

func NewTriangle(a, b, c Point) Triangle {
	return Triangle{a, b, c}
}

// Check if triangle has given Point as one of its three points
func (t Triangle) Has(p Point) bool {
	return t[0].Equals(p) || t[1].Equals(p) || t[2].Equals(p)
}

// Get the lines that make up this triangle
func (t Triangle) Lines() Lines {
	return Lines{
		Line{t[0], t[1]},
		Line{t[1], t[2]},
		Line{t[2], t[0]},
	}
}

func (t Triangle) Points() Points {
	return Points{t[0], t[1], t[2]}
}

func (t Triangle) Center() Point {
	points := make(Points, 3)
	for i, pt := range t {
		points[i] = pt
	}
	return points.Center()
}

func (t Triangle) IsValid() bool {
	if t[0].Equals(t[1]) {
		return false
	}
	if t[1].Equals(t[2]) {
		return false
	}
	if t[0].Equals(t[2]) {
		return false
	}

	return true
}

func (t Triangle) String() string {
	return fmt.Sprintf("%s %s %s", t[0].String(), t[1].String(), t[2].String())
}

// check if given line intersects with triangle
func (t Triangle) LineIntersects(line Line) bool {
	var angleA, angleB, angleC float64
	var side1, side2, side3 float64

	// FIRST CHECK
	// Check that our 2D triangle (t), in 3D space, is in between our
	// the two points of our line. We accomplish this by making a new triangle
	// with the two points of our line (line), and a point in our triangle (t). If
	// the angle at our line points (line) are less than or equal to 90 degrees, we
	// know the line (line) MAY pass through the triangle (t). Must be true
	// one time, out of the three points from our triangle (t).
	found := false
	for i, _ := range t {
		side1 = line[0].Distance(line[1])
		side2 = line[0].Distance(t[i])
		side3 = line[1].Distance(t[i])
		angleA, angleB, angleC = findAngles(side1, side2, side3)
		// check if our line (line) passes through one of the points of our
		// triangle (t), therefore giving us a straight line or collinear situation
		min, _ := minMax(angleA, angleB, angleC)
		if min == 0 {
			// Is collinear
			return false
		}
		// since side2/3 are the lines from t[0] to line1 and t[0] to line2, we
		// want to check the opposite angle of those sides do not exceed 90
		// degrees (which is angleB and angleC)
		if angleB <= 90.0 && angleC <= 90.0 {
			found = true
			break
		}
	}
	if !found {
		return false
	}

	// SECOND CHECK
	// Make sure the angle of any (and all) two points on our triangle (t) and
	// a single point in our line, are less than or equal to 90 degress. If all
	// three composited triangles meet this criteria, our line is bisecting our
	// triangle (t)
	for i := range t {
		i2 := (i + 1) % 3
		angleA, angleB, _ = findAngles(line[0].Distance(t[i]), line[0].Distance(t[i2]), t[i].Distance(t[i2]))
		if angleA > 90 || angleB > 90 {
			return false
		}
	}

	return true
}

func average(xs ...float64) float64 {
	total := 0.0
	for _, v := range xs {
		total += v
	}
	return total / float64(len(xs))
}

func minMax(array ...float64) (float64, float64) {
	var max float64 = array[0]
	var min float64 = array[0]
	for _, value := range array {
		if max < value {
			max = value
		}
		if min > value {
			min = value
		}
	}
	return min, max
}

/*
func triangleArea(a, b, c float64) float64 {
	cosGama := (c*c - a*a - b*b) / (2 * a * b)
	sinGama := math.Sqrt(1.0 - cosGama*cosGama)
	heightA := b * sinGama
	return (a * heightA) / 2
}
*/

// findAngle - with given three sides of a triangle, calculate an angle
func findAngles(a, b, c float64) (float64, float64, float64) {
	numerator := math.Pow(a, 2) + math.Pow(b, 2) - math.Pow(c, 2)
	denomerator := 2 * b * a
	rads := math.Acos(numerator / denomerator)
	angleA := radiansToDegrees(rads)

	numerator = math.Pow(b, 2) + math.Pow(c, 2) - math.Pow(a, 2)
	denomerator = 2 * b * c
	rads = math.Acos(numerator / denomerator)
	angleB := radiansToDegrees(rads)

	angleC := 180.0 - (angleA + angleB)
	return angleB, angleC, angleA
}

// convert from radians to degrees
func radiansToDegrees(r float64) float64 {
	degrees := r / Degree

	// Doing this weird casting thing because doing findAngle with (4,4,4) as
	// the parameters, the end response is 59.999999, and I want 60.0
	str := fmt.Sprintf("%f", degrees)
	degrees, _ = strconv.ParseFloat(str, 64)

	return degrees
}

func degreesToRadians(r float64) float64 {
	return r * math.Pi / 180
}
