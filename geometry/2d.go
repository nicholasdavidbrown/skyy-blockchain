package geometry

import (
	"math"

	mapset "github.com/deckarep/golang-set"
	"gitlab.com/skyy.network/skyy-blockchain/units"
)

func TwoDimensional(loc1, loc2 units.Location, padding units.Measurement, fillType Fill, limit int) units.Locations {

	latRange := units.RangeLatitudes(
		units.Latitudes{loc1.Latitude, loc2.Latitude},
	)
	longRange := units.RangeLongitudes(
		units.Longitudes{loc1.Longitude, loc2.Longitude},
	)
	altRange := units.RangeAltitudes(
		units.Altitudes{loc1.Altitude, loc2.Altitude},
	)

	_, longest := minMaxInt(len(latRange), len(longRange), len(altRange))

	locales := make(units.Locations, longest)
	for i := 0; i < len(locales); i++ {
		locales[i] = units.NewLocation(
			latRange[getIndex(i, len(latRange), longest)],
			longRange[getIndex(i, len(longRange), longest)],
			altRange[getIndex(i, len(altRange), longest)],
		)
	}

	set := mapset.NewSet()
	for _, loc := range locales {
		oneDimensional(set, loc, padding, fillType, limit)
	}
	return setToLocations(set)
}

// This is meant to equalize the fact that we can have different length ranges
// between lat/long/alts. So we need to "fill out" when one has too few.
func getIndex(x, actualLength, desiredLength int) int {
	if desiredLength == actualLength {
		return x
	}
	if desiredLength < actualLength {
		return -1
	}

	multiplier := desiredLength / actualLength
	modulus := desiredLength % actualLength

	// create an []int the length of actualLength length, with alias to where in the
	// actualLength the value is
	var virtualArray []int
	for i := 0; i < actualLength; i++ {
		for j := 0; j < multiplier; j++ {
			virtualArray = append(virtualArray, i)
		}
		if modulus > 0 && modNum(i, actualLength) <= (modulus-1) {
			virtualArray = append(virtualArray, i)
		}
	}

	return virtualArray[x]
}

// returns a number based on its location, middle-out
// 0 --> 4
// 1 --> 2
// 2 --> 0
// 3 --> 1
// 4 --> 3
func modNum(y, actualLength int) int {
	y += 1
	middle := int(math.Ceil(float64(actualLength) / 2.0))
	distanceFromMiddle := int(math.Abs(float64(middle) - float64(y)))
	value := distanceFromMiddle * 2
	if y > middle {
		value -= 1
	}
	return value
}

func minMaxInt(array ...int) (int, int) {
	var max int = array[0]
	var min int = array[0]
	for _, value := range array {
		if max < value {
			max = value
		}
		if min > value {
			min = value
		}
	}
	return min, max
}
