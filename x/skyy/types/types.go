package types

import (
	"fmt"
	"strings"
	"time"

	"github.com/google/uuid"

	"gitlab.com/skyy.network/skyy-blockchain/geometry"
	"gitlab.com/skyy.network/skyy-blockchain/units"
)

type Triangle [3]string

type Triangles []Triangle

func (p Triangle) String() string {
	return fmt.Sprintf("(%s %s %s)", p[0], p[1], p[2])
}

type AirspaceType string

const (
	Reservation  AirspaceType = "reservation"
	Permissioned AirspaceType = "permissioned"
)

type Polyhedron struct {
	UID        string       `json:"uid"`
	Type       AirspaceType `json:"type"`
	StartTime  time.Time    `json:"start_time"`
	ExpireTime time.Time    `json:"expire_time"`
	Triangles  Triangles    `json:"triangles"`
}

type Polyhedrons []Polyhedron

func NewPolyhedron(typ AirspaceType, triangles Triangles, start, expire time.Time) Polyhedron {
	// if we are permissioned airspace, there is no start/expire times. Set to zero
	if typ == Permissioned {
		start = time.Time{}
		expire = time.Time{}
	}
	return Polyhedron{
		UID:        uuid.New().String(),
		Type:       typ,
		StartTime:  start,
		ExpireTime: expire,
		Triangles:  triangles,
	}
}

func (p Polyhedron) IsType(t AirspaceType) bool {
	return p.Type == t
}

func (p1 Polyhedron) TimeOverlap(p2 Polyhedron) bool {
	if p1.IsType(Permissioned) || p2.IsType(Permissioned) {
		return true
	}

	// TODO No conflict when is same vehicle
	if p1.StartTime.Unix() > p2.ExpireTime.Unix() || p1.ExpireTime.Unix() < p2.StartTime.Unix() {
		return false
	}

	return true
}

func (p Polyhedron) IsEmpty() bool {
	return p.UID == "" || len(p.Triangles) == 0
}

func (p Polyhedron) IsExpired() bool {
	if p.IsType(Permissioned) {
		return false
	}
	return time.Now().Unix() > p.ExpireTime.Unix()
}

func (p Polyhedron) ContainsTime(t time.Time) bool {
	if p.IsType(Permissioned) {
		return true
	}
	ts := t.Unix()
	return ts >= p.StartTime.Unix() && ts <= p.ExpireTime.Unix()
}

// Convert Vertices from strings to unit.Locations
func (p Polyhedron) Locations() (units.Locations, error) {
	locales := make(units.Locations, 0)
	for _, triangle := range p.Triangles {
		for _, vert := range triangle {
			loc, err := units.ParseLocation(vert)
			if err != nil {
				return nil, err
			}
			locales = append(locales, loc)
		}
	}
	return locales.Unique(), nil
}

func (p Polyhedron) GeoPolyhedron() geometry.Polyhedron {
	return geometry.Polyhedron(p.GeoTriangles())
}

func (p Polyhedron) GeoTriangles() (tris geometry.Triangles) {
	for _, tri := range p.Triangles {
		loc, _ := units.ParseLocation(tri[0])
		p1 := geometry.NewPoint(
			loc.Latitude.Float64(),
			loc.Longitude.Float64(),
			loc.Altitude.Measurement.Amount,
		)
		loc, _ = units.ParseLocation(tri[1])
		p2 := geometry.NewPoint(
			loc.Latitude.Float64(),
			loc.Longitude.Float64(),
			loc.Altitude.Measurement.Amount,
		)
		loc, _ = units.ParseLocation(tri[2])
		p3 := geometry.NewPoint(
			loc.Latitude.Float64(),
			loc.Longitude.Float64(),
			loc.Altitude.Measurement.Amount,
		)
		var geoTri geometry.Triangle
		geoTri[0] = p1
		geoTri[1] = p2
		geoTri[2] = p3
		tris = append(tris, geoTri)
	}

	return
}

func (p Polyhedron) String() string {
	return fmt.Sprintf("%s | %s --> %s", p.Triangles, p.StartTime, p.ExpireTime)
}

// Sector is a section of earth (lat/long/alt) to store a listing of polyhedron
// UIDs for search capabilities.
type Sector struct {
	Polyhedrons []string `json:"polyhedron_uids"`
}

func NewSector(uids []string) Sector {
	return Sector{
		Polyhedrons: uids,
	}
}

func (s Sector) String() string {
	return strings.Join(s.Polyhedrons, ", ")
}

type Vehicle struct {
	ID               string    `json:"id"`
	Class            string    `json:"class"`
	Operator         string    `json:"operator"`
	RegistrationDate time.Time `json:"registration_date"`
}

func NewVehicle(id string) Vehicle {
	return Vehicle{
		ID:               id,
		RegistrationDate: time.Now(),
	}
}

func (v Vehicle) Key() string {
	return v.ID
}

func (v Vehicle) String() string {
	return v.ID
}

func (a AirspaceType) String() string {
	return string(a)
}
