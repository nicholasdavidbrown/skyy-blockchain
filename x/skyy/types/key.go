package types

const (
	// module cmname
	ModuleName = "skyy"

	// StoreKey to be used when creating the KVStore
	StoreKey = ModuleName
)
