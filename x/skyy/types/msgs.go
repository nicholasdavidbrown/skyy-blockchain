package types

import (
	"fmt"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"gitlab.com/skyy.network/skyy-blockchain/geometry"
)

const RouterKey = ModuleName // this was defined in your key.go file
const maxSeconds = 3600

// MsgSetPolyhedron defines a SetPolyhedron message
type MsgSetPolyhedron struct {
	Polyhedron Polyhedron     `json:"polyhedron"`
	Signer     sdk.AccAddress `json:"signer"`
}

// NewMsgSetPolyhedron is a constructor function for MsgSetPolyhedron
func NewMsgSetPolyhedron(hedron Polyhedron, signer sdk.AccAddress) MsgSetPolyhedron {
	return MsgSetPolyhedron{
		Polyhedron: hedron,
		Signer:     signer,
	}
}

// Route should return the cmname of the module
func (msg MsgSetPolyhedron) Route() string { return RouterKey }

// Type should return the action
func (msg MsgSetPolyhedron) Type() string { return "set_polyhedron" }

// ValidateBasic runs stateless checks on the message
func (msg MsgSetPolyhedron) ValidateBasic() sdk.Error {
	if msg.Signer.Empty() {
		return sdk.ErrInvalidAddress(msg.Signer.String())
	}
	if len(msg.Polyhedron.Triangles) == 0 {
		return sdk.ErrUnknownRequest("Must have at least one pyramid")
	}
	for _, triangle := range msg.Polyhedron.Triangles {
		if len(triangle) != 3 {
			return sdk.ErrUnknownRequest("Invalid Triangle: Must have exactly 3 vertices")
		}
	}
	if msg.Polyhedron.Type == "" {
		return sdk.ErrUnknownRequest(
			fmt.Sprintf("Invalid Polyhedron: Must have airspace type"),
		)
	}
	if msg.Polyhedron.IsEmpty() {
		return sdk.ErrUnknownRequest(
			fmt.Sprintf("Invalid Polyhedron: Cannot be empty"),
		)
	}
	if msg.Polyhedron.IsType(Reservation) && msg.Polyhedron.StartTime.IsZero() {
		return sdk.ErrUnknownRequest(
			fmt.Sprintf("Invalid Polyhedron: Start: %s. Error: Cannot be zero", msg.Polyhedron.StartTime),
		)
	}
	if msg.Polyhedron.IsType(Reservation) && msg.Polyhedron.ExpireTime.IsZero() {
		return sdk.ErrUnknownRequest(
			fmt.Sprintf("Invalid Polyhedron: End: %s. Error: Cannot be zero", msg.Polyhedron.ExpireTime),
		)
	}
	if msg.Polyhedron.ExpireTime.Unix() < msg.Polyhedron.StartTime.Unix() {
		return sdk.ErrUnknownRequest(
			fmt.Sprintf("Invalid Polyhedron: End: %s. Error: Cannot be before start time", msg.Polyhedron.ExpireTime),
		)
	}
	if msg.Polyhedron.IsExpired() {
		return sdk.ErrUnknownRequest(
			fmt.Sprintf("Invalid Polyhedron: End: %s. Error: Cannot be in the past", msg.Polyhedron.ExpireTime),
		)
	}
	if msg.Polyhedron.IsType(Reservation) && msg.Polyhedron.ExpireTime.Sub(msg.Polyhedron.StartTime).Seconds() > maxSeconds {
		return sdk.ErrUnknownRequest(
			fmt.Sprintf("Invalid Polyhedron: Error: Cannot reserve more than %d seconds", maxSeconds),
		)
	}
	_, err := geometry.NewPolyhedron(msg.Polyhedron.GeoTriangles())
	if err != nil {
		return sdk.ErrUnknownRequest(
			fmt.Sprintf("Invalid Polyhedron: Error: %s", err.Error()),
		)
	}
	return nil
}

// GetSignBytes encodes the message for signing
func (msg MsgSetPolyhedron) GetSignBytes() []byte {
	return sdk.MustSortJSON(ModuleCdc.MustMarshalJSON(msg))
}

// GetSigners defines whose signature is required
func (msg MsgSetPolyhedron) GetSigners() []sdk.AccAddress {
	return []sdk.AccAddress{msg.Signer}
}
