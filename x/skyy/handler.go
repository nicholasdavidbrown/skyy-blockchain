package skyy

import (
	"fmt"
	"time"

	sdk "github.com/cosmos/cosmos-sdk/types"
)

// NewHandler returns a handler for "skyy" type messages.
func NewHandler(keeper Keeper) sdk.Handler {
	return func(ctx sdk.Context, msg sdk.Msg) sdk.Result {
		switch msg := msg.(type) {
		case MsgSetPolyhedron:
			result := handleMsgSetPolyhedron(ctx, keeper, msg)
			fmt.Printf("RESULT: %+v\n", result)
			return result
		default:
			errMsg := fmt.Sprintf("Unrecognized skyy Msg type: %v", msg.Type())
			return sdk.ErrUnknownRequest(errMsg).Result()
		}
	}
}

func handleMsgSetPolyhedron(ctx sdk.Context, keeper Keeper, msg MsgSetPolyhedron) sdk.Result {
	var err error
	// this set is used to track which UIDs we've already checked
	set := make(map[string]bool)

	locs, err := msg.Polyhedron.Locations()
	if err != nil {
		return sdk.ErrUnknownRequest(
			fmt.Sprintf("Error parsing location: %s", err.Error()),
		).Result()
	}

	var timestamps []time.Time
	// start with an empty timestamp. This is where we store permissioned
	// airspace aka airports
	timestamps = append(timestamps, time.Time{})

	// round down the start time to nearest hour
	roundedTs := time.Date(msg.Polyhedron.StartTime.Year(), msg.Polyhedron.StartTime.Month(), msg.Polyhedron.StartTime.Day(), msg.Polyhedron.StartTime.Hour(), 0, 0, 0, msg.Polyhedron.StartTime.Location())
	for ts := roundedTs; ts.Unix() <= msg.Polyhedron.ExpireTime.Unix(); ts = ts.Add(1 * time.Hour) {
		timestamps = append(timestamps, ts)
	}

	// check for conflicts
	if msg.Polyhedron.IsType(AirspaceReservation) {
		for depth := 2; depth >= -1; depth-- {
			sectorCount := locs.SectorCount(depth)
			// if there are too many sectors skip, and continue to try a larger sector
			if sectorCount > 10 && depth > -1 {
				continue
			}
			for _, ts := range timestamps {
				for _, sectorLoc := range locs.SectorLocations(depth) {
					sector := keeper.GetSector(ctx, sectorLoc, ts, depth)

					// filter out expired UIDs
					for _, uid := range sector.Polyhedrons {
						// check if we've already checked this polygon
						if set[uid] {
							continue
						}
						set[uid] = true // update our set

						poly := keeper.GetPolyhedron(ctx, uid)
						if !poly.IsEmpty() && !poly.IsExpired() && msg.Polyhedron.TimeOverlap(poly) {
							// Check if any of the points are inside the other polyhedron
							polyhedron1 := msg.Polyhedron.GeoPolyhedron()
							polyhedron2 := poly.GeoPolyhedron()
							for _, pt := range polyhedron1.Points() {
								if polyhedron2.ContainsPoint(pt) {
									fmt.Println("CONFLICT")
									return sdk.ErrUnknownRequest("Conflict").Result()
								}
							}
							for _, pt := range polyhedron2.Points() {
								if polyhedron1.ContainsPoint(pt) {
									fmt.Println("CONFLICT")
									return sdk.ErrUnknownRequest("Conflict").Result()
								}
							}

							// check for intersecting polyhedrons
							source := msg.Polyhedron.GeoTriangles() // geometry triangles of source
							target := poly.GeoTriangles()           // geometry triangles of target
							for _, sourceTri := range source {
								lines := sourceTri.Lines() // get lines of source
								for _, targetTri := range target {
									for _, line := range lines {
										if targetTri.LineIntersects(line) {
											fmt.Println("CONFLICT")
											return sdk.ErrUnknownRequest("Conflict").Result()
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	// update db with our new polyhedron
	keeper.SetPolyhedron(ctx, msg.Polyhedron)

	// update sectors
	for _, ts := range timestamps {
		for depth := 2; depth >= -1; depth-- {
			for _, sectorLoc := range locs.SectorLocations(depth) {
				sector := keeper.GetSector(ctx, sectorLoc, ts, depth)
				sector.Polyhedrons = append(sector.Polyhedrons, msg.Polyhedron.UID)
				keeper.SetSector(ctx, sectorLoc, ts, depth, sector)
			}
		}
	}

	return sdk.Result{}
}
