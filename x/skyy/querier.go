package skyy

import (
	"time"

	"github.com/cosmos/cosmos-sdk/codec"
	"gitlab.com/skyy.network/skyy-blockchain/units"

	sdk "github.com/cosmos/cosmos-sdk/types"
	geojson "github.com/paulmach/go.geojson"
	abci "github.com/tendermint/tendermint/abci/types"
)

// query endpoints supported by the skyy Querier
const (
	QuerySector = "sectors"
)

// NewQuerier is the module level router for state queries
func NewQuerier(keeper Keeper) sdk.Querier {
	return func(ctx sdk.Context, path []string, req abci.RequestQuery) (res []byte, err sdk.Error) {
		switch path[0] {
		case QuerySector:
			return querySector(ctx, path[1:], req, keeper)
		default:
			return nil, sdk.ErrUnknownRequest("unknown skyy query endpoint")
		}
	}
}

func querySector(ctx sdk.Context, path []string, req abci.RequestQuery, keeper Keeper) ([]byte, sdk.Error) {
	var err error
	var format string
	set := make(map[string]bool)

	ts, err := time.Parse(time.RFC3339, path[0])
	if err != nil {
		return nil, sdk.ErrUnknownRequest(err.Error())
	}

	format = path[1]
	path = path[2:]

	locs := make(units.Locations, len(path))
	for i, locStr := range path {
		locs[i], err = units.ParseLocation(locStr)
		if err != nil {
			return nil, sdk.ErrUnknownRequest(err.Error())
		}
	}

	// we include zero time in our query list to check permissioned airspace
	timestamps := []time.Time{time.Time{}, ts}

	var polyhedrons Polyhedrons
	for depth := 2; depth >= -1; depth-- {
		sectorCount := locs.SectorCount(depth)
		// if there are too many sectors skip, and continue to try a larger sector
		if sectorCount > 10 && depth > -1 {
			continue
		}

		for _, timestamp := range timestamps {
			for _, loc := range locs.SectorLocations(depth) {
				sector := keeper.GetSector(ctx, loc, timestamp, depth)

				for _, uid := range sector.Polyhedrons {
					// check if we've already checked this polygon
					if set[uid] {
						continue
					}
					set[uid] = true // update our set

					poly := keeper.GetPolyhedron(ctx, uid)
					// skip any that are empty, or does not contain given time
					if !poly.IsEmpty() && poly.ContainsTime(ts) {
						polyhedrons = append(polyhedrons, poly)
					}
				}
			}
		}
		break
	}

	var res []byte
	switch format {
	case "geojson":
		features := geojson.NewFeatureCollection()
		for _, poly := range polyhedrons {
			locs, _ := poly.Locations()
			feature := geojson.NewPolygonFeature([][][]float64{locs.GeoJsonPoints()})
			feature.ID = poly.UID
			feature.Properties["start_time"] = poly.StartTime
			feature.Properties["expire_time"] = poly.ExpireTime
			features = features.AddFeature(feature)
		}
		res, err = codec.MarshalJSONIndent(keeper.cdc, features)
		if err != nil {
			panic("could not marshal result to GEOJSON")
		}
	default:
		res, err = codec.MarshalJSONIndent(keeper.cdc, polyhedrons)
		if err != nil {
			panic("could not marshal result to JSON")
		}
	}

	return res, nil
}
