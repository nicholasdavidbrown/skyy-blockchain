package skyy

import (
	"fmt"
	"time"

	"github.com/cosmos/cosmos-sdk/codec"
	"github.com/cosmos/cosmos-sdk/x/bank"
	"gitlab.com/skyy.network/skyy-blockchain/units"

	sdk "github.com/cosmos/cosmos-sdk/types"
)

type prefixKey string

const (
	polyhedronKey prefixKey = "p_"
	sectorKey     prefixKey = "s_"
)

// Keeper maintains the link to data storage and exposes getter/setter methods for the various parts of the state machine
type Keeper struct {
	coinKeeper bank.Keeper

	storeKey sdk.StoreKey // Unexposed key to access store from sdk.Context

	cdc *codec.Codec // The wire codec for binary encoding/decoding.
}

func getKey(k string, prefix prefixKey) string {
	return fmt.Sprintf("%s%s", prefix, k)
}

func getSectorKey(loc units.Location, ts time.Time, depth int) string {
	// round down to the nearest hour
	t := ts.Unix() / 3600
	return fmt.Sprintf("%s|%d|%d", loc.String(), depth, t)
}

// NewKeeper creates new instances of the skyy Keeper
func NewKeeper(coinKeeper bank.Keeper, storeKey sdk.StoreKey, cdc *codec.Codec) Keeper {
	return Keeper{
		coinKeeper: coinKeeper,
		storeKey:   storeKey,
		cdc:        cdc,
	}
}

func (k Keeper) SetPolyhedron(ctx sdk.Context, poly Polyhedron) {
	if poly.UID == "" {
		return
	}
	key := getKey(poly.UID, polyhedronKey)

	store := ctx.KVStore(k.storeKey)
	store.Set([]byte(key), k.cdc.MustMarshalBinaryBare(poly))
}

// Get an iterator for polyhedrons
func (k Keeper) GetPolyhedronIterator(ctx sdk.Context) sdk.Iterator {
	store := ctx.KVStore(k.storeKey)
	return sdk.KVStorePrefixIterator(store, []byte(polyhedronKey))
}

func (k Keeper) GetPolyhedron(ctx sdk.Context, key string) Polyhedron {
	key = getKey(key, polyhedronKey)
	store := ctx.KVStore(k.storeKey)
	if !store.Has([]byte(key)) {
		return Polyhedron{}
	}
	bz := store.Get([]byte(key))
	var poly Polyhedron
	k.cdc.MustUnmarshalBinaryBare(bz, &poly)

	// check if polyhedron has expired
	if poly.IsExpired() {
		return Polyhedron{}
	}

	return poly
}

// Get an iterator for sectors
func (k Keeper) GetSectorIterator(ctx sdk.Context) sdk.Iterator {
	store := ctx.KVStore(k.storeKey)
	return sdk.KVStorePrefixIterator(store, []byte(sectorKey))
}

func (k Keeper) GetSector(ctx sdk.Context, loc units.Location, ts time.Time, depth int) Sector {
	key := getKey(getSectorKey(loc, ts, depth), sectorKey)
	store := ctx.KVStore(k.storeKey)
	if !store.Has([]byte(key)) {
		return Sector{}
	}
	bz := store.Get([]byte(key))
	var sector Sector
	k.cdc.MustUnmarshalBinaryBare(bz, &sector)

	return sector
}

func (k Keeper) SetSector(ctx sdk.Context, loc units.Location, ts time.Time, depth int, sector Sector) {
	key := getKey(getSectorKey(loc, ts, depth), sectorKey)

	store := ctx.KVStore(k.storeKey)
	store.Set([]byte(key), k.cdc.MustMarshalBinaryBare(sector))
}

// Get an iterator
func (k Keeper) GetIterator(ctx sdk.Context) sdk.Iterator {
	store := ctx.KVStore(k.storeKey)
	return sdk.KVStorePrefixIterator(store, nil)
}
