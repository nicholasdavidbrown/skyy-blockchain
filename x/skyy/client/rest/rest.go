package rest

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/cosmos/cosmos-sdk/client/context"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/rest"
	"github.com/cosmos/cosmos-sdk/x/auth/client/utils"
	"github.com/gorilla/mux"

	"gitlab.com/skyy.network/skyy-blockchain/geometry"
	"gitlab.com/skyy.network/skyy-blockchain/units"
	"gitlab.com/skyy.network/skyy-blockchain/x/skyy/types"
)

// RegisterRoutes - Central function to define routes that get registered by the main application
func RegisterRoutes(cliCtx context.CLIContext, r *mux.Router, storeName string) {

	// Health Check Endpoint
	r.HandleFunc(fmt.Sprintf("/%s/ping", storeName), pingHandler(cliCtx, storeName)).Methods("GET")

	r.HandleFunc(fmt.Sprintf("/%s/sector", storeName), getSectorHandler(cliCtx, storeName)).Methods("GET")

	r.HandleFunc(fmt.Sprintf("/%s/polyhedron", storeName), reservePolyhedronHandler(cliCtx)).Methods("POST")
	r.HandleFunc(fmt.Sprintf("/%s/cylinder", storeName), reserveCylinderHandler(cliCtx)).Methods("POST")
}

// CORS support
func enableCors(w *http.ResponseWriter, r *http.Request) {
	origin := r.Header.Get("Origin")
	(*w).Header().Set("Access-Control-Allow-Origin", origin)
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Credentials", "true")
	(*w).Header().Set("Access-Control-Allow-Headers", "Authorization,Accept,Origin,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range")
	(*w).Header().Set("Access-Control-Allow-Methods ", "GET,POST,OPTIONS,PUT,DELETE,PATCH")
}

// --------------------------------------------------------------------------------
// Tx Handler

type reserveCylinder struct {
	BaseReq rest.BaseReq `json:"base_req"`
	// VehicleID  string       `json:"vehicle"`
	StartTime  time.Time `json:"start_time"`
	ExpireTime time.Time `json:"expire_time"`
	Location   string    `json:"location"`
	Radius     string    `json:"radius"`
	Height     string    `json:"height"`
}

func reserveCylinderHandler(cliCtx context.CLIContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w, r)
		var req reserveCylinder
		var err error

		if !rest.ReadRESTReq(w, r, cliCtx.Codec, &req) {
			rest.WriteErrorResponse(w, http.StatusBadRequest, "failed to parse request")
			return
		}

		baseReq := req.BaseReq.Sanitize()
		if !baseReq.ValidateBasic(w) {
			return
		}
		// set fees
		baseReq.Fees, err = sdk.ParseCoins("1skyy")
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusInternalServerError, err.Error())
			return
		}

		signer, err := sdk.AccAddressFromBech32(req.BaseReq.From)
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		loc, err := units.ParseLocation(req.Location)
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		radius, err := units.ParseMeasurement(req.Radius)
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}
		rad := units.Latitude(0).ConverstionRatio(radius, units.Ground)

		height, err := units.ParseMeasurement(req.Height)
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		point := geometry.NewPoint(loc.Latitude.Float64(), loc.Longitude.Float64(), loc.Altitude.Measurement.Amount)
		p, err := geometry.NewPolyhedronFromCylinder(point, rad, height.Amount)
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		triangles := make(types.Triangles, len(p))
		for i, tri := range p {
			t := types.Triangle{}
			for j, pt := range tri {
				t[j] = units.NewLocation(
					units.Latitude(pt.X),
					units.Longitude(pt.Y),
					units.NewAltitude(
						units.NewMeasurement(pt.Z, loc.Altitude.Measurement.Unit),
						loc.Altitude.Level,
					),
				).String()
			}
			triangles[i] = t
		}

		poly := types.NewPolyhedron(
			types.Reservation,
			triangles,
			req.StartTime,
			req.ExpireTime,
		)

		msg := types.NewMsgSetPolyhedron(
			poly,
			signer,
		)
		err = msg.ValidateBasic()
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		utils.WriteGenerateStdTxResponse(w, cliCtx, baseReq, []sdk.Msg{msg})
	}
}

type reservePolyhedron struct {
	BaseReq rest.BaseReq `json:"base_req"`
	// VehicleID  string       `json:"vehicle"`
	StartTime  time.Time  `json:"start_time"`
	ExpireTime time.Time  `json:"expire_time"`
	Triangles  [][]string `json:"triangles"`
}

func reservePolyhedronHandler(cliCtx context.CLIContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w, r)
		var req reservePolyhedron
		var err error

		if !rest.ReadRESTReq(w, r, cliCtx.Codec, &req) {
			rest.WriteErrorResponse(w, http.StatusBadRequest, "failed to parse request")
			return
		}

		baseReq := req.BaseReq.Sanitize()
		if !baseReq.ValidateBasic(w) {
			return
		}
		// set fees
		baseReq.Fees, err = sdk.ParseCoins("1skyy")
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusInternalServerError, err.Error())
			return
		}

		signer, err := sdk.AccAddressFromBech32(req.BaseReq.From)
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		// check we can parse locations
		var triangles types.Triangles
		for _, points := range req.Triangles {
			var tri types.Triangle
			for i, point := range points[:3] {
				_, err = units.ParseLocation(point)
				if err != nil {
					rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
					return
				}
				tri[i] = point
			}
			triangles = append(triangles, tri)
		}

		poly := types.NewPolyhedron(
			types.Reservation,
			triangles,
			req.StartTime,
			req.ExpireTime,
		)

		msg := types.NewMsgSetPolyhedron(
			poly,
			signer,
		)
		err = msg.ValidateBasic()
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		utils.WriteGenerateStdTxResponse(w, cliCtx, baseReq, []sdk.Msg{msg})
	}
}

//------------------------------------------------------------------------
// Query Handlers

// Ping - endpoint to check that the API is up and available
func pingHandler(cliCtx context.CLIContext, storeName string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w, r)
		fmt.Fprintf(w, string(`{"ping":"pong"}`))
	}
}

func getSectorHandler(cliCtx context.CLIContext, storeCmName string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w, r)
		var err error

		points, ok := r.URL.Query()["point"]

		if !ok || len(points) < 1 {
			err := fmt.Errorf("Url Param 'point' is missing")
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		// get format type, default to json
		format := "json"
		formats := r.URL.Query()["format"]
		if len(formats) > 0 {
			format = formats[0]
		}

		ts, ok := r.URL.Query()["ts"]
		if !ok || len(ts) < 1 {
			err := fmt.Errorf("Url Param 'ts' is missing")
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		_, err = time.Parse(time.RFC3339, ts[0])
		if err != nil {
			err = fmt.Errorf("Invalid timestamp format: %s", err.Error())
			rest.WriteErrorResponse(w, http.StatusBadRequest, err.Error())
			return
		}

		vertices := strings.Join(points[:], "/")

		res, _, err := cliCtx.QueryWithData(fmt.Sprintf("custom/%s/sectors/%s/%s/%s", storeCmName, ts[0], format, vertices), nil)
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusNotFound, err.Error())
			return
		}

		rest.PostProcessResponse(w, cliCtx, res)

	}
}
