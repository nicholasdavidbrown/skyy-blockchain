package cli

import (
	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/codec"
	"github.com/spf13/cobra"
	"gitlab.com/skyy.network/skyy-blockchain/x/skyy/types"
)

func GetQueryCmd(storeKey string, cdc *codec.Codec) *cobra.Command {
	skyyQueryCmd := &cobra.Command{
		Use:                        types.ModuleName,
		Short:                      "Querying commands for the skyy module",
		DisableFlagParsing:         true,
		SuggestionsMinimumDistance: 2,
		RunE:                       client.ValidateCmd,
	}
	skyyQueryCmd.AddCommand(client.GetCommands()...)
	return skyyQueryCmd
}
