package cli

import (
	"time"

	"github.com/spf13/cobra"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/client/context"
	"github.com/cosmos/cosmos-sdk/codec"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/x/auth"
	"github.com/cosmos/cosmos-sdk/x/auth/client/utils"

	"gitlab.com/skyy.network/skyy-blockchain/geometry"
	"gitlab.com/skyy.network/skyy-blockchain/units"
	"gitlab.com/skyy.network/skyy-blockchain/x/skyy/types"
)

func GetTxCmd(storeKey string, cdc *codec.Codec) *cobra.Command {
	skyyTxCmd := &cobra.Command{
		Use:                        types.ModuleName,
		Short:                      "skyy transaction subcommands",
		DisableFlagParsing:         true,
		SuggestionsMinimumDistance: 2,
		RunE:                       client.ValidateCmd,
	}

	skyyTxCmd.AddCommand(client.PostCommands(
		GetCmdAddCylinder(cdc),
	)...)

	return skyyTxCmd
}

// GetCmdAddCylinder command to add cylinder shaped polyhedron
func GetCmdAddCylinder(cdc *codec.Codec) *cobra.Command {
	return &cobra.Command{
		Use:   "add-cylinder [location] [radius] [height] [type] [start] [expire]",
		Short: "add cylinder shaped polyhedron",
		Args:  cobra.MinimumNArgs(4),
		RunE: func(cmd *cobra.Command, args []string) error {
			cliCtx := context.NewCLIContext().WithCodec(cdc)
			txBldr := auth.NewTxBuilderFromCLI().WithTxEncoder(utils.GetTxEncoder(cdc))

			loc, err := units.ParseLocation(args[0])
			if err != nil {
				return err
			}

			radius, err := units.ParseMeasurement(args[1])
			if err != nil {
				return err
			}
			rad := units.Latitude(0).ConverstionRatio(radius, units.Ground)

			height, err := units.ParseMeasurement(args[2])
			if err != nil {
				return err
			}

			typ := types.Reservation
			if args[3] == types.Permissioned.String() {
				typ = types.Permissioned
			}

			var start, expire time.Time
			if len(args) > 4 {
				start, err = time.Parse(time.RFC3339, args[4])
				if err != nil {
					return err
				}
			}
			if len(args) > 5 {
				expire, err = time.Parse(time.RFC3339, args[5])
				if err != nil {
					return err
				}
			}

			point := geometry.NewPoint(loc.Latitude.Float64(), loc.Longitude.Float64(), loc.Altitude.Measurement.Amount)
			p, err := geometry.NewPolyhedronFromCylinder(point, rad, height.Amount)
			if err != nil {
				return err
			}

			triangles := make(types.Triangles, len(p))
			for i, tri := range p {
				t := types.Triangle{}
				for j, pt := range tri {
					t[j] = units.NewLocation(
						units.Latitude(pt.X),
						units.Longitude(pt.Y),
						units.NewAltitude(
							units.NewMeasurement(pt.Z, loc.Altitude.Measurement.Unit),
							loc.Altitude.Level,
						),
					).String()
				}
				triangles[i] = t
			}

			poly := types.NewPolyhedron(
				typ,
				triangles,
				start,
				expire,
			)

			msg := types.NewMsgSetPolyhedron(
				poly,
				cliCtx.GetFromAddress(),
			)
			err = msg.ValidateBasic()
			if err != nil {
				return err
			}

			return utils.GenerateOrBroadcastMsgs(cliCtx, txBldr, []sdk.Msg{msg})
		},
	}
}
