package skyy

import (
	"gitlab.com/skyy.network/skyy-blockchain/x/skyy/types"
)

const (
	ModuleName           = types.ModuleName
	RouterKey            = types.RouterKey
	StoreKey             = types.StoreKey
	AirspaceReservation  = types.Reservation
	AirspacePermissioned = types.Permissioned
)

var (
	NewMsgSetPolyhedron = types.NewMsgSetPolyhedron
	NewPolyhedron       = types.NewPolyhedron
	ModuleCdc           = types.ModuleCdc
	RegisterCodec       = types.RegisterCodec
)

type (
	MsgSetPolyhedron = types.MsgSetPolyhedron
	Triangle         = types.Triangle
	Triangles        = types.Triangles
	Polyhedron       = types.Polyhedron
	Polyhedrons      = types.Polyhedrons
	Sector           = types.Sector
)
