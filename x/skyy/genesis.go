package skyy

import (
	"strconv"
	"strings"
	"time"

	sdk "github.com/cosmos/cosmos-sdk/types"
	abci "github.com/tendermint/tendermint/abci/types"
	"gitlab.com/skyy.network/skyy-blockchain/units"
)

type sector struct {
	Location string    `json:"location"`
	Time     time.Time `json:"timestamp"`
	Depth    string    `json:"depth"`
	Sector   Sector    `json:"sector"`
}

type GenesisState struct {
	Polyhedrons Polyhedrons `json:"polyhedrons"`
	Sectors     []sector    `json:"sectors"`
}

func NewGenesisState(polys Polyhedrons, secs []sector) GenesisState {
	return GenesisState{
		Polyhedrons: polys,
		Sectors:     secs,
	}
}

func ValidateGenesis(data GenesisState) error {
	return nil
}

func DefaultGenesisState() GenesisState {
	return GenesisState{}
}

func InitGenesis(ctx sdk.Context, keeper Keeper, data GenesisState) []abci.ValidatorUpdate {
	for _, poly := range data.Polyhedrons {
		keeper.SetPolyhedron(ctx, poly)
	}

	for _, sec := range data.Sectors {
		loc, err := units.ParseLocation(sec.Location)
		if err != nil {
			panic(err)
		}

		depth, err := strconv.Atoi(sec.Depth)
		if err != nil {
			panic(err)
		}
		keeper.SetSector(ctx, loc, sec.Time, depth, sec.Sector)
	}

	return []abci.ValidatorUpdate{}
}

func ExportGenesis(ctx sdk.Context, k Keeper) GenesisState {
	var polys Polyhedrons
	iterator := k.GetPolyhedronIterator(ctx)
	for ; iterator.Valid(); iterator.Next() {
		var poly Polyhedron
		k.cdc.MustUnmarshalBinaryBare(iterator.Value(), &poly)
		polys = append(polys, poly)
	}
	iterator.Close()

	var sectors []sector
	iterator = k.GetSectorIterator(ctx)
	for ; iterator.Valid(); iterator.Next() {
		var sec sector
		parts := strings.Split(strings.TrimLeft(string(iterator.Key()), string(sectorKey)), "|")
		sec.Location = parts[0]

		epoch, err := strconv.ParseInt(parts[1], 10, 64)
		if err != nil {
			panic(err)
		}
		sec.Time = time.Unix(epoch*3600, 0)

		sec.Depth = parts[2]
		k.cdc.MustUnmarshalBinaryBare(iterator.Value(), &sec.Sector)
		sectors = append(sectors, sec)
	}
	iterator.Close()

	return GenesisState{
		Polyhedrons: polys,
		Sectors:     sectors,
	}
}
