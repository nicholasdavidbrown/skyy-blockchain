[![pipeline status](https://gitlab.com/skyy.network/skyynet/skyy-blockchain/badges/master/pipeline.svg)](https://gitlab.com/skyy.network/skyynet/skyy-blockchain/commits/master)
[![coverage report](https://gitlab.com/skyy.network/skyynet/skyy-blockchain/badges/master/coverage.svg)](https://gitlab.com/skyy.network/skyynet/skyy-blockchain/commits/master)

Skyy Network Blockchain
=======================
Airspace control on a Cosmos Blockchain

Skyy.Network is a blockchain to manage airspace and reserve airspace for all
sorts of vehicles, including airplanes and drones.

## Setup
Ensure you have a recent version of go (ie `1.121) and enabled go modules
```
export GO111MODULE=on
```
And have `GOBIN` in your `PATH`
```
export GOBIN=$GOPATH/bin
```

### Automated Install
To install easily, run the following command...
```bash
make setup
```

### Manual Install
Install via this `make` command.

```bash
make install
```

Once you've installed `skyycli` and `skyyd`, check that they are there.

```bash
skyycli help
skyyd help
```

### Configuration

Next configure your chain.
```bash
# Initialize configuration files and genesis file
# moniker is the name of your node
skyyd init <moniker> --chain-id skyyNetwork


# Copy the Address output here and save it for later use
# [optional] add "--ledger" at the end to use a Ledger Nano S
skyycli keys add jack

# Copy the Address output here and save it for later use
skyycli keys add alice

# Add both accounts, with coins to the genesis file
skyyd add-genesis-account $(skyycli keys show jack -a) 1000skyy,100000000stake
skyyd add-genesis-account $(skyycli keys show alice -a) 1000skyy,100000000stake

# Configure your CLI to eliminate need for chain-id flag
skyycli config chain-id skyyNetwork
skyycli config output json
skyycli config indent true
skyycli config trust-node true

skyyd gentx --name jack
```

## Start
There are three services you may want to start.

#### Daemon
This runs the backend
```bash
make start
```

#### REST API Service
Starts an HTTP service to service requests to the backend.
```bash
make start-rest
```
