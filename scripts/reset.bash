#!/bin/sh

set -x
set -e

while true; do
  make install
  skyyd init local --chain-id skyyNetwork

  skyyd add-genesis-account $(skyycli keys show jack -a) 1000skyy,100000000stake
  skyyd add-genesis-account $(skyycli keys show alice -a) 1000skyy,100000000stake


  for i in {1..100}
  do
      skyyd add-genesis-account $(skyycli keys show "batch$i" -a) 1000skyy,100000000stake
  done

  skyycli config chain-id skyyNetwork
  skyycli config output json
  skyycli config indent true
  skyycli config trust-node true

  echo "password" | skyyd gentx --name jack
  skyyd collect-gentxs
  skyyd validate-genesis

  break

done
