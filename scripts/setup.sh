#!/bin/sh

set -x
set -e

while true; do
  make install
  skyyd init local --chain-id skyyNetwork

  echo "password" | skyycli keys add jack
  echo "password" | skyycli keys add alice

  skyyd add-genesis-account $(skyycli keys show jack -a) 1000skyy,100000000stake
  skyyd add-genesis-account $(skyycli keys show alice -a) 1000skyy,100000000stake

  skyycli config chain-id skyyNetwork
  skyycli config output json
  skyycli config indent true
  skyycli config trust-node true

  echo "password" | skyyd gentx --name jack
  skyyd collect-gentxs
  skyyd validate-genesis

  break

done
