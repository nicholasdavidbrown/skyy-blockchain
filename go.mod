module gitlab.com/skyy.network/skyy-blockchain

go 1.13

require (
	github.com/btcsuite/btcd v0.0.0-20190824003749-130ea5bddde3 // indirect
	github.com/cosmos/cosmos-sdk v0.37.3
	github.com/deckarep/golang-set v1.7.1
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.0
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/paulmach/go.geojson v1.4.0
	github.com/pelletier/go-toml v1.4.0 // indirect
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.4.0
	github.com/tendermint/go-amino v0.15.0
	github.com/tendermint/tendermint v0.32.6
	github.com/tendermint/tm-db v0.2.0
	golang.org/x/sys v0.0.0-20190329044733-9eb1bfa1ce65 // indirect
	google.golang.org/genproto v0.0.0-20190327125643-d831d65fe17d // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127
)
