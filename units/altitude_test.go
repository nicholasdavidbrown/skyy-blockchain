package units

import (
	. "gopkg.in/check.v1"
)

type AltitudeSuite struct{}

var _ = Suite(&AltitudeSuite{})

func (s *AltitudeSuite) TestNew(c *C) {
	a := NewAltitude(
		NewMeasurement(5, Feet),
		AGL,
	)
	c.Check(a.Measurement.Equals(NewMeasurement(5, Feet)), Equals, true)
	c.Check(a.Level, Equals, AGL)
}

func (s *AltitudeSuite) TestSnap(c *C) {
	a := NewAltitude(
		NewMeasurement(5, Feet),
		AMSL,
	)
	a = a.Snap()
	c.Check(a.Measurement.Equals(NewMeasurement(5, Meters)), Equals, true, Commentf("%+v", a.Measurement))
	c.Check(a.Level, Equals, AMSL)
	a = a.SnapUp()
	c.Check(a.Measurement.Equals(NewMeasurement(15, Meters)), Equals, true, Commentf("%+v", a.Measurement))
	a = a.SnapDown()
	c.Check(a.Measurement.Equals(NewMeasurement(5, Meters)), Equals, true, Commentf("%+v", a.Measurement))

	a = NewAltitude(
		NewMeasurement(1006, Meters),
		AMSL,
	)
	a = a.Snap()
	c.Check(a.Measurement.Equals(NewMeasurement(1005, Meters)), Equals, true, Commentf("%+v", a.Measurement))
	c.Check(a.Level, Equals, AMSL)

	a = NewAltitude(
		NewMeasurement(1, Meters),
		AMSL,
	)
	a = a.SnapDown()
	c.Check(a.Measurement.Equals(NewMeasurement(-5, Meters)), Equals, true, Commentf("%+v", a.Measurement))

}

func (s *AltitudeSuite) TestString(c *C) {
	a := NewAltitude(
		NewMeasurement(5, Feet),
		AMSL,
	)

	c.Check(a.String(), Equals, "5' AMSL")
}

func (s *AltitudeSuite) TestParse(c *C) {
	alt, err := ParseAltitude("5m AGL")
	c.Assert(err, IsNil)
	c.Check(alt.Measurement.Equals(NewMeasurement(5, Meters)), Equals, true)
	c.Check(alt.Level, Equals, AGL)

	alt, err = ParseAltitude("5m")
	c.Assert(err, IsNil)
	c.Check(alt.Measurement.Equals(NewMeasurement(5, Meters)), Equals, true)
	c.Check(alt.Level, Equals, AMSL)
}

func (s *AltitudeSuite) TestAltRange(c *C) {
	low := NewAltitude(
		NewMeasurement(60, Meters),
		AMSL,
	)
	high := low.SnapAndStep(3)

	altitudes := RangeAltitudes(Altitudes{high, low})
	c.Assert(altitudes, HasLen, 4, Commentf("Len: %d", len(altitudes)))
	for i, _ := range altitudes {
		c.Check(altitudes[i], Equals, low.SnapAndStep(i))
	}
}
