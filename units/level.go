package units

import (
	"strings"
)

// A unit of measure relative distance from the "ground"
type Level string

var (
	AMSL Level = "AMSL" // Above Mean Sea Level
	AGL  Level = "AGL"  // Above Ground Level
)

func StringToLevel(s string) Level {
	switch strings.ToUpper(s) {
	case "AMSL":
		return AMSL
	case "AGL":
		return AGL
	default:
		// default to AMSL
		return AMSL
	}
}
