package units

import (
	"fmt"
	"math"
	"strconv"
)

var EarthRadius Measurement = Measurement{
	Amount: 6371000,
	Unit:   Meters,
}

// circumference of the earth = 40030173.592
// We want the blocks to be 10m in size
const BlocksPerAltitude = 40030173.592 / 10.0

const pi = math.Pi // PI 3.14159....

const longStep = float64(360) / float64(BlocksPerAltitude)
const longHalfStep = longStep / float64(2) // midway point within a sector
const longDecimal = 6                      // float precision of longitude

const latStep = float64(180) / float64(BlocksPerAltitude)
const latHalfStep = latStep / float64(2) // midway point within a sector
// latitude precision is higher than long due to it have fewer total degrees
// (180 vs 360)
const latDecimal = 7 // float precision of latitude

type Longitude float64
type Longitudes []Longitude
type Latitude float64
type Latitudes []Latitude

func NewLongitude(f float64) (Longitude, error) {
	if f > 180 || f < -180 {
		return 0, fmt.Errorf("Invalid longitude (-180/180): %f", f)
	}
	return Longitude(roundToDecimalPoints(f, longDecimal)), nil
}

func ParseLongitude(s string) (Longitude, error) {
	f, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return 0, err
	}
	return NewLongitude(f)
}

func (l Longitude) ConverstionRatio(mm Measurement, alt Altitude) float64 {
	return converstionRatio(mm, alt, 360)
}

// Snap to nearest Longitude anchor
func (l Longitude) Snap() Longitude {
	return l.SnapAndStep(0)
}

// Snap to nearest Longitude anchor, +1
func (l Longitude) SnapUp() Longitude {
	return l.SnapAndStep(1)
}

// Snap to nearest Longitude anchor, -1
func (l Longitude) SnapDown() Longitude {
	return l.SnapAndStep(-1)
}

// Snap to nearest Longitude anchor and step
func (l Longitude) SnapAndStep(incr int) Longitude {
	return Longitude(snap(float64(l), longStep, longHalfStep, incr, longDecimal))
}

func (l Longitude) Float64() float64 {
	return float64(l)
}

func (l Longitudes) MinMax() (Longitude, Longitude) {
	floats := make([]float64, len(l))
	for i, v := range l {
		floats[i] = float64(v)
	}

	min, max := minMax(floats)
	return Longitude(min), Longitude(max)
}

func RangeLongitudes(input Longitudes) (longitudes Longitudes) {
	low, high := input.MinMax()
	low = low.Snap()
	high = high.Snap()

	current := low
	for current <= high {
		longitudes = append(longitudes, current)
		current = current.SnapUp()
	}

	return
}
func NewLatitude(f float64) (Latitude, error) {
	if f > 90 || f < -90 {
		return 0, fmt.Errorf("Invalid latitude (-90/90): %f", f)
	}
	return Latitude(roundToDecimalPoints(f, latDecimal)), nil
}

func ParseLatitude(s string) (Latitude, error) {
	f, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return 0, err
	}
	return NewLatitude(f)
}

func (l Latitude) ConverstionRatio(mm Measurement, alt Altitude) float64 {
	return converstionRatio(mm, alt, 180)
}

// Snap to nearest Latitude anchor
func (l Latitude) Snap() Latitude {
	return l.SnapAndStep(0)
}

// Snap to nearest Latitude anchor, +1
func (l Latitude) SnapUp() Latitude {
	return l.SnapAndStep(1)
}

// Snap to nearest Latitude anchor, -1
func (l Latitude) SnapDown() Latitude {
	return l.SnapAndStep(-1)
}

// Snap to nearest Latitude anchor and step
func (l Latitude) SnapAndStep(incr int) Latitude {
	return Latitude(snap(float64(l), latStep, latHalfStep, incr, latDecimal))
}

func (l Latitude) Float64() float64 {
	return float64(l)
}

func (l Latitudes) MinMax() (Latitude, Latitude) {
	floats := make([]float64, len(l))
	for i, v := range l {
		floats[i] = float64(v)
	}

	min, max := minMax(floats)
	return Latitude(min), Latitude(max)
}

func RangeLatitudes(input Latitudes) (latitudes Latitudes) {
	low, high := input.MinMax()
	low = low.Snap()
	high = high.Snap()

	current := low
	for current <= high {
		latitudes = append(latitudes, current)
		current = current.SnapUp()
	}

	return
}

// snaps long/lat (l), to nearest long/lat and increment by (incr) which gives
// us the ability to snap to the nearest, to neighbors in either direction, etc
func snap(l, step, halfStep float64, incr int, decimal int) float64 {
	lower := float64(int(l/step)) * step
	return roundToDecimalPoints(lower+(float64(incr)*step)+halfStep, decimal)
}

func converstionRatio(mm Measurement, alt Altitude, degrees int) float64 {
	alt = alt.ConvertAMSL()
	alt.Measurement = alt.Measurement.Convert(mm.Unit)

	radius := alt.Measurement.Add(EarthRadius)
	circumference := GetCircumference(radius.Amount)

	ratio := float64(degrees) / circumference
	return ratio * mm.Amount
}

func roundToDecimalPoints(f float64, p int) float64 {
	pow := math.Pow(10, float64(p))
	return math.Round(f*pow) / pow
}

// Calculate the circumference of a circle with the given radius
func GetCircumference(r float64) float64 {
	return 2.0 * pi * r
}
