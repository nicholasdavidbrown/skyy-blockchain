package units

import (
	"fmt"
	"regexp"
	"strconv"
)

// One meter in Feet
const oneMeterInFeet = 3.28084

var parsePrefix = regexp.MustCompile(`^[0-9,\.]+`)
var parseSuffix = regexp.MustCompile(`[',a-z,A-Z]`)

type Measurement struct {
	Amount float64 `json:"amount"`
	Unit   Unit    `json:"unit"`
}

func NewMeasurement(amount float64, unit Unit) Measurement {
	return Measurement{
		Amount: amount,
		Unit:   unit,
	}
}

func ParseMeasurement(s string) (Measurement, error) {
	numStr := parsePrefix.FindAllString(s, 1)
	if len(numStr) == 0 {
		return Measurement{}, fmt.Errorf("No amount detected in measurement")
	}

	f, err := strconv.ParseFloat(numStr[0], 64)
	if err != nil {
		return Measurement{}, err
	}

	suffixStr := parseSuffix.FindAllString(s, 1)
	if len(suffixStr) == 0 {
		return Measurement{}, fmt.Errorf("No suffix detected in measurement")
	}

	u := StringToUnit(suffixStr[0])
	return NewMeasurement(f, u), nil
}

func (m Measurement) Add(mm Measurement) Measurement {
	mm = mm.Convert(m.Unit)
	return NewMeasurement(m.Amount+mm.Amount, m.Unit)
}

func (m Measurement) Subtract(mm Measurement) Measurement {
	mm = mm.Convert(m.Unit)
	return NewMeasurement(m.Amount-mm.Amount, m.Unit)
}

func (m Measurement) Divide(mm Measurement) Measurement {
	mm = mm.Convert(m.Unit)
	return NewMeasurement(m.Amount/mm.Amount, m.Unit)
}

func (m Measurement) Multiply(mm Measurement) Measurement {
	mm = mm.Convert(m.Unit)
	return NewMeasurement(m.Amount*mm.Amount, m.Unit)
}

func (m Measurement) Convert(unit Unit) Measurement {
	if m.Unit == unit {
		return m
	}
	if m.Unit == Meters && unit == Feet {
		return NewMeasurement(
			(m.Amount * oneMeterInFeet),
			unit,
		)
	} else if m.Unit == Feet && unit == Meters {
		return NewMeasurement(
			(m.Amount / oneMeterInFeet),
			unit,
		)
	} else if unit == Lat || unit == Long {
		panic(fmt.Errorf("Improper function (use ConvertWithAltitude): %s", unit))
	} else {
		panic(fmt.Errorf("Unsupported Measurement Unit: %s", unit))
	}
}

func (m Measurement) Equals(mm Measurement) bool {
	if mm.Unit != m.Unit {
		mm = mm.Convert(m.Unit)
	}
	return m.Amount == mm.Amount
}

func (m Measurement) GreaterThan(mm Measurement) bool {
	if mm.Unit != m.Unit {
		mm = mm.Convert(m.Unit)
	}
	return m.Amount > mm.Amount
}

func (m Measurement) GreaterThanOrEqualTo(mm Measurement) bool {
	if mm.Unit != m.Unit {
		mm = mm.Convert(m.Unit)
	}
	return m.Amount >= mm.Amount
}

func (m Measurement) LessThan(mm Measurement) bool {
	if mm.Unit != m.Unit {
		mm = mm.Convert(m.Unit)
	}
	return m.Amount < mm.Amount
}

func (m Measurement) LessThanOrEqualTo(mm Measurement) bool {
	if mm.Unit != m.Unit {
		mm = mm.Convert(m.Unit)
	}
	return m.Amount <= mm.Amount
}

func (m Measurement) String() string {
	return fmt.Sprintf("%g%s", m.Amount, m.Unit)
}
