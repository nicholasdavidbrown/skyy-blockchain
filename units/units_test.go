package units

import (
	"testing"

	. "gopkg.in/check.v1"
)

func TestPackage(t *testing.T) { TestingT(t) }

type UnitsSuite struct{}

var _ = Suite(&UnitsSuite{})

func (s *UnitsSuite) TestRounding(c *C) {
	c.Check(roundDownDecimalPoints(125.539858, 2), Equals, 125.53)
	c.Check(roundDownDecimalPoints(125.539858, 1), Equals, 125.5)
	c.Check(roundDownDecimalPoints(125.539858, 0), Equals, 125.0)
	c.Check(roundDownDecimalPoints(125.539858, -1), Equals, 120.0)
	c.Check(roundDownDecimalPoints(125.539858, -2), Equals, 100.0)

	c.Check(roundDownDecimalPoints(1.13, 2), Equals, 1.13)
}
