package units

import (
	. "gopkg.in/check.v1"
)

type LatLongSuite struct{}

var _ = Suite(&LatLongSuite{})

func (s *LatLongSuite) TestNew(c *C) {
	long, err := NewLongitude(400)
	c.Assert(err, NotNil)
	c.Check(long, Equals, Longitude(0))
	long, err = NewLongitude(120.578)
	c.Assert(err, IsNil)
	c.Check(long, Equals, Longitude(120.578))

	lat, err := NewLatitude(400)
	c.Assert(err, NotNil)
	c.Check(lat, Equals, Latitude(0))
	lat, err = NewLatitude(43.578)
	c.Assert(err, IsNil)
	c.Check(lat, Equals, Latitude(43.578))
}

func (s *LatLongSuite) TestSnap(c *C) {
	// mid range
	long, err := NewLongitude(120.578000)
	c.Assert(err, IsNil)
	snap := long.Snap()
	c.Check(snap, Equals, snap.SnapUp().SnapDown())
}

func (s *LatLongSuite) TestLatRange(c *C) {
	low := Latitude(23)
	high := low.SnapAndStep(3)

	latitudes := RangeLatitudes(Latitudes{low, high})
	c.Assert(latitudes, HasLen, 4)
	for i, _ := range latitudes {
		c.Check(latitudes[i], Equals, low.SnapAndStep(i))
	}
}

func (s *LatLongSuite) TestLongRange(c *C) {
	low := Longitude(23)
	high := low.SnapAndStep(3)

	longitudes := RangeLongitudes(Longitudes{high, low})
	c.Assert(longitudes, HasLen, 4)
	for i, _ := range longitudes {
		c.Check(longitudes[i], Equals, low.SnapAndStep(i))
	}
}

func (s *LatLongSuite) TestParsing(c *C) {
	long, err := ParseLongitude("3.45")
	c.Assert(err, IsNil)
	c.Check(long.Float64(), Equals, 3.45)

	lat, err := ParseLatitude("3.45")
	c.Assert(err, IsNil)
	c.Check(lat.Float64(), Equals, 3.45)

	_, err = ParseLatitude("bad string")
	c.Assert(err, NotNil)
}

func (s *LatLongSuite) TestConversionRatio(c *C) {
	mm := NewMeasurement(1, Meters)
	alt := NewAltitude(NewMeasurement(0, Meters), AMSL)
	ratio := Longitude(0).ConverstionRatio(mm, alt)

	c.Check(ratio, Equals, 0.000008993216059187306)

	mm = mm.Convert(Feet)
	ratio = Longitude(0).ConverstionRatio(mm, alt)
	c.Check(ratio, Equals, 0.000008993216059187306)

	mm = NewMeasurement(3, Feet)
	ratio = Longitude(0).ConverstionRatio(mm, alt)
	c.Check(ratio, Equals, 0.000008223396501372184)
}

func (s *LatLongSuite) TestMinMax(c *C) {
	fs := []float64{3.4, 4.6, 10, 1.4}
	min, max := minMax(fs)

	c.Check(min, Equals, 1.4)
	c.Check(max, Equals, 10.0)
}

func (s *LatLongSuite) TestRounding(c *C) {
	c.Check(roundToDecimalPoints(5.55555, 1), Equals, 5.6)
}
