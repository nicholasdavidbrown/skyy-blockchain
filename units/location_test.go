package units

import (
	. "gopkg.in/check.v1"
)

type LocationSuite struct{}

var _ = Suite(&LocationSuite{})

var makeLoc = func(lat, lng, alt float64) Location {
	return NewLocation(
		Latitude(lat),
		Longitude(lng),
		NewAltitude(
			NewMeasurement(alt, Meters),
			AMSL,
		),
	)
}

func (s *LocationSuite) TestNew(c *C) {
	lat, _ := NewLatitude(5.6)
	long, _ := NewLongitude(4.5)
	l := NewLocation(
		lat,
		long,
		NewAltitude(
			NewMeasurement(5, Meters),
			AMSL,
		),
	)

	c.Check(l.Latitude.Float64(), Equals, 5.6)
	c.Check(l.Longitude.Float64(), Equals, 4.5)
	c.Check(l.Altitude.Measurement.Equals(NewMeasurement(5, Meters)), Equals, true)
	c.Check(l.Altitude.Level, Equals, AMSL)
	f := l.Floats()
	c.Check(f[0], Equals, 5.6)
	c.Check(f[1], Equals, 4.5)
}

func (s *LocationSuite) TestString(c *C) {
	lat, _ := NewLatitude(5.6)
	long, _ := NewLongitude(4.5)
	l := NewLocation(
		lat,
		long,
		NewAltitude(
			NewMeasurement(5, Meters),
			AMSL,
		),
	)

	c.Check(l.String(), Equals, "5.6,4.5,5m AMSL")

	l.Longitude = Longitude(1.13)
	c.Check(l.String(), Equals, "5.6,1.13,5m AMSL")
}

func (s *LocationSuite) TestParse(c *C) {
	locale, err := ParseLocation("-45.81,120.4985,400' AMSL")
	c.Assert(err, IsNil)
	c.Check(locale.Latitude.Float64(), Equals, -45.81)
	c.Check(locale.Longitude.Float64(), Equals, 120.4985)
	c.Check(
		locale.Altitude.Measurement.Equals(NewMeasurement(400, Feet)),
		Equals, true,
	)
	c.Check(locale.Altitude.Level, Equals, AMSL)

	locale, err = ParseLocation("-45.81,120.4985,400'")
	c.Assert(err, IsNil)
	c.Check(locale.Latitude.Float64(), Equals, -45.81)
	c.Check(locale.Longitude.Float64(), Equals, 120.4985)
	c.Check(
		locale.Altitude.Measurement.Equals(NewMeasurement(400, Feet)),
		Equals, true,
	)
	c.Check(locale.Altitude.Level, Equals, AMSL)

}

func (s *LocationSuite) TestDistance(c *C) {

	loc1 := makeLoc(1, 1, 1)
	loc2 := makeLoc(2, 2, 2)

	distance := loc1.Distance(loc2)
	c.Assert(roundToDecimalPoints(distance.Amount, 3), Equals, 248639.532)

	loc1 = makeLoc(0, 0.0000001, 0)
	loc2 = makeLoc(0, 0.0000002, 0)
	distance = loc1.Distance(loc2)
	c.Assert(roundToDecimalPoints(distance.Amount, 3), Equals, 0.011)

	loc1 = makeLoc(0, 0.0000001, 0)
	loc2 = makeLoc(0, 0.0000002, 100)
	distance = loc1.Distance(loc2)
	c.Assert(roundToDecimalPoints(distance.Amount, 3), Equals, 100.0)
}

func (s *LocationSuite) TestBounds(c *C) {
	locs := make(Locations, 8)
	locs[0] = makeLoc(1, 1, 1)
	locs[1] = makeLoc(1, 1, 1)
	locs[2] = makeLoc(2, 3, 4)
	locs[3] = makeLoc(5, 5, 7)
	locs[4] = makeLoc(-1, 0, -1)
	locs[5] = makeLoc(10, 100, 10)
	locs[6] = makeLoc(6, 1, 9)
	locs[7] = makeLoc(0, 0, -0)

	bounds := locs.Bounds()
	c.Check(bounds[0].Equals(makeLoc(-1, 0, -1)), Equals, true)
	c.Check(bounds[1].Equals(makeLoc(-1, 0, 10)), Equals, true)
	c.Check(bounds[2].Equals(makeLoc(-1, 100, -1)), Equals, true)
	c.Check(bounds[3].Equals(makeLoc(10, 0, 10)), Equals, true)
	c.Check(bounds[4].Equals(makeLoc(10, 100, -1)), Equals, true)
	c.Check(bounds[5].Equals(makeLoc(10, 100, 10)), Equals, true)
}

func (s *LocationSuite) TestCenter(c *C) {
	locs := make(Locations, 4)
	locs[0] = makeLoc(0, 0, 12)
	locs[1] = makeLoc(0, 2, 12)
	locs[2] = makeLoc(2, 0, 24)
	locs[3] = makeLoc(2, 2, 24)

	center := locs.Center()

	c.Check(
		center.Equals(makeLoc(1, 1, 18)),
		Equals,
		true,
		Commentf("%s", center.String()),
	)
}

func (s *LocationSuite) TestUnique(c *C) {
	locs := make(Locations, 3)
	locs[0] = makeLoc(0, 0, 0)
	locs[1] = makeLoc(0, 2, 0)
	locs[2] = makeLoc(0, 0, 0)

	locs = locs.Unique()
	c.Assert(locs, HasLen, 2)
	c.Check(locs[0].Equals(makeLoc(0, 0, 0)), Equals, true)
	c.Check(locs[1].Equals(makeLoc(0, 2, 0)), Equals, true)
}

func (s *LocationSuite) TestSectorLocations(c *C) {
	locs := make(Locations, 4)
	locs[0] = makeLoc(1.12, 1.13, 0)
	locs[1] = makeLoc(1.12, 1.11, 0)
	locs[2] = makeLoc(1.10, 1.10, 0)
	locs[3] = makeLoc(1.11, 1.12, 0)

	sectors := locs.SectorLocations(2)
	/*
		for _, sector := range sectors {
			fmt.Println(sector.String())
		}
	*/
	c.Assert(sectors, HasLen, 12, Commentf("Len: %d", len(sectors)))

	c.Check(locs.SectorCount(2), Equals, 12)
}

func (s *LocationSuite) TestHasOverlap(c *C) {
	//c.Skip("Skipping until distance is correct")
	// two of the same polygon
	locs1 := Locations{
		makeLoc(0, 0, 100),
		makeLoc(2, 0, 100),
		makeLoc(0, 2, 100),
		makeLoc(2, 2, 100),
	}
	c.Assert(locs1.HasOverlap(locs1), Equals, true)

	// No overlap
	locs1 = Locations{
		makeLoc(0, 0, 0),
		makeLoc(1, 0, 0),
		makeLoc(0, 1, 0),
		makeLoc(1, 1, 0),
	}
	locs2 := Locations{
		makeLoc(2, 2, 0),
		makeLoc(3, 2, 0),
		makeLoc(2, 3, 0),
		makeLoc(3, 3, 0),
	}
	c.Assert(locs1.HasOverlap(locs2), Equals, false)

	// Has overlap
	locs1 = Locations{
		makeLoc(0, 0, 0),
		makeLoc(2, 0, 0),
		makeLoc(0, 2, 0),
		makeLoc(2, 2, 0),
	}
	locs2 = Locations{
		makeLoc(1, 1, 0),
		makeLoc(3, 1, 0),
		makeLoc(1, 3, 0),
		makeLoc(3, 3, 0),
	}
	c.Assert(locs1.HasOverlap(locs2), Equals, true)

	// No overlap
	locs1 = Locations{
		makeLoc(0, 0, 100000000),
		makeLoc(2, 0, 100000000),
		makeLoc(0, 2, 100000000),
		makeLoc(2, 2, 100000000),
	}
	locs2 = Locations{
		makeLoc(1, 1, 0),
		makeLoc(3, 1, 0),
		makeLoc(1, 3, 0),
		makeLoc(3, 3, 0),
	}
	c.Assert(locs1.HasOverlap(locs2), Equals, false)
}
