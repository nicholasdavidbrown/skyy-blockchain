package units

import (
	"fmt"
	"math"
	"reflect"
	"sort"
	"strings"
)

// A 1d point in airspace
type Location struct {
	Latitude  Latitude  `json:"latitude"`
	Longitude Longitude `json:"longitude"`
	Altitude  Altitude  `json:"altitude"`
}

func (l Location) Floats() []float64 {
	alt := l.Altitude.ConvertAMSL()
	m := alt.Measurement.Convert(Feet)
	return []float64{
		l.Latitude.Float64(),
		l.Longitude.Float64(),
		m.Amount,
	}
}

func (l Location) GeoJsonPoint() []float64 {
	alt := l.Altitude.ConvertAMSL()
	m := alt.Measurement.Convert(Feet)
	return []float64{
		l.Longitude.Float64(),
		l.Latitude.Float64(),
		m.Amount,
	}
}

type Locations []Location

func (locs Locations) LatFloats() []float64 {
	fs := make([]float64, len(locs))
	for i, f := range locs {
		fs[i] = f.Latitude.Float64()
	}
	return fs
}

func (locs Locations) LngFloats() []float64 {
	fs := make([]float64, len(locs))
	for i, f := range locs {
		fs[i] = f.Longitude.Float64()
	}
	return fs
}

func (locs Locations) AltFloats() []float64 {
	fs := make([]float64, len(locs))
	for i, f := range locs {
		fs[i] = f.Altitude.Measurement.Convert(
			locs[0].Altitude.Measurement.Unit,
		).Amount
	}
	return fs
}

// Converts series of locations into a series of slice of floats
func (locs Locations) Floats() [][]float64 {
	locs = locs.Unique()
	poly := make([][]float64, len(locs))
	for i, loc := range locs {
		poly[i] = loc.Floats()
	}
	return poly
}

// Converts series of locations into a series of slice of floats
func (locs Locations) GeoJsonPoints() [][]float64 {
	locs = locs.Unique()
	poly := make([][]float64, len(locs))
	for i, loc := range locs {
		poly[i] = loc.GeoJsonPoint()
	}
	return poly
}

// Returns a list of unique locations
func (locs Locations) Unique() Locations {
	keys := make(map[string]bool)
	list := Locations{}
	for _, loc := range locs {
		if _, value := keys[loc.String()]; !value {
			keys[loc.String()] = true
			list = append(list, loc)
		}
	}
	return list
}

// Finds the center location of series of locations
func (locs Locations) Center() Location {
	alt := NewAltitude(
		NewMeasurement(
			average(locs.AltFloats()),
			locs[0].Altitude.Measurement.Unit,
		),
		locs[0].Altitude.Level,
	)
	return NewLocation(
		Latitude(average(locs.LatFloats())),
		Longitude(average(locs.LngFloats())),
		alt,
	)
}

func (locs Locations) SectorCount(roundingNum int) int {
	locales := locs.Bounds()

	// convert bounds to sector locations
	for i, locale := range locales {
		locales[i] = NewLocation(
			Latitude(roundDownDecimalPoints(locale.Latitude.Float64(), roundingNum)),
			Longitude(roundDownDecimalPoints(locale.Longitude.Float64(), roundingNum)),
			locale.Altitude.ClassAltitude(),
		)
	}

	lats := locales.LatFloats()
	lngs := locales.LngFloats()

	minLat, maxLat := minMax(lats)
	minLng, maxLng := minMax(lngs)

	incr := math.Pow(10, float64(roundingNum)) / math.Pow(100, float64(roundingNum))
	latd := math.Round((maxLat-minLat)*100) / 100
	lngd := math.Round((maxLng-minLng)*100) / 100

	return (int(math.Ceil((latd / incr))) + 1) * (int(math.Ceil((lngd / incr))) + 1)
}

// Return a list of lat/long/alt "sectors" these locations are in
/*
	decimal  decimal     distance
	places   degrees    (in meters)
	-------  ---------  -----------
	  1      0.1000000  11,057.43      11 km
	  2      0.0100000   1,105.74       1 km
	  3      0.0010000     110.57
	  4      0.0001000      11.06
	  5      0.0000100       1.11
	  6      0.0000010       0.11      11 cm
	  7      0.0000001       0.01       1 cm
*/
func (locs Locations) SectorLocations(roundingNum int) Locations {
	// TODO: should include altitude in sectors. This should separate between
	// different class of vehicles. Higher altitude vehicles will less of them,
	// but over a larger area of airspace. While lower altitude vehicle (like
	// drones) will have more of them, but with a smaller airspace (ie won't
	// fly as far).
	locales := locs.Bounds()

	// convert bounds to sector locations
	for i, locale := range locales {
		locales[i] = NewLocation(
			Latitude(roundDownDecimalPoints(locale.Latitude.Float64(), roundingNum)),
			Longitude(roundDownDecimalPoints(locale.Longitude.Float64(), roundingNum)),
			locale.Altitude.ClassAltitude(),
		)
	}

	// find all sector locations between these sector locations
	lats := locales.LatFloats()
	lngs := locales.LngFloats()
	// alts := locales.AltFloats()

	minLat, maxLat := minMax(lats)
	minLng, maxLng := minMax(lngs)
	// minAlt, maxAlt := minMax(alts)

	var sectors Locations
	incr := math.Pow(10, float64(roundingNum)) / math.Pow(100, float64(roundingNum))
	lat := minLat
	lng := minLng
	for {
		sectors = append(
			sectors,
			NewLocation(
				Latitude(lat),
				Longitude(lng),
				locales[0].Altitude,
			),
		)

		// if we've hit our last lat/lng, exit now.
		if lat >= maxLat && lng >= maxLng {
			break
		}

		if lng < maxLng {
			lng = math.Round((lng+incr)*100) / 100
		} else {
			lat = math.Round((lat+incr)*100) / 100
			lng = minLng // reset lng back to min
		}
	}

	return sectors
}

// Returns a bounding box around the given locations
func (locs Locations) Bounds() Locations {
	// TODO: must be at least 4 unique locations
	if len(locs) == 0 {
		return nil
	}
	// get our unit of measurement and lvl from our first location of locations
	mUnit := locs[0].Altitude.Measurement.Unit
	mLvl := locs[0].Altitude.Level

	lats := locs.LatFloats()
	lngs := locs.LngFloats()
	alts := locs.AltFloats()

	minLat, maxLat := minMax(lats)
	minLng, maxLng := minMax(lngs)
	minAlt, maxAlt := minMax(alts)

	makeLoc := func(lat, lng, alt float64) Location {
		return NewLocation(
			Latitude(lat),
			Longitude(lng),
			NewAltitude(
				NewMeasurement(alt, mUnit),
				mLvl,
			),
		)
	}

	// create the 6 corners of our block
	corners := make(Locations, 6)
	corners[0] = makeLoc(minLat, minLng, minAlt)
	corners[1] = makeLoc(minLat, minLng, maxAlt)
	corners[2] = makeLoc(minLat, maxLng, minAlt)
	corners[3] = makeLoc(maxLat, minLng, maxAlt)
	corners[4] = makeLoc(maxLat, maxLng, minAlt)
	corners[5] = makeLoc(maxLat, maxLng, maxAlt)

	return corners
}

func (locs1 Locations) HasOverlap(locs2 Locations) bool {
	return locs1.hasOverlap(locs2) || locs2.hasOverlap(locs1)
}

func (locs1 Locations) hasOverlap(locs2 Locations) bool {
	// check that locs1 is not the same as locs2
	if len(locs1) == len(locs2) {
		locs1String := make([]string, len(locs1))
		locs2String := make([]string, len(locs2))
		for i, _ := range locs1 {
			locs1String[i] = locs1[i].String()
			locs2String[i] = locs2[i].String()
		}

		sort.Slice(locs1String, func(i, j int) bool {
			return locs1String[i] > locs1String[j]
		})
		sort.Slice(locs2String, func(i, j int) bool {
			return locs2String[i] > locs2String[j]
		})

		if reflect.DeepEqual(locs1String, locs2String) {
			return true
		}
	}

	// get target sum of locs1 (by adding distance to each point from center
	// (aka known point inside the polygon). Any distance that add up larger
	// than this will be outside the polygon (and therefore not overlap)
	var targetSum float64
	for _, loc := range locs1 {
		targetSum += locs1[0].Distance(loc).Amount
	}

	for _, targetLoc := range locs2 {
		var sum float64
		for _, loc := range locs1 {
			sum += targetLoc.Distance(loc).Amount
		}
		if sum < targetSum {
			return true
		}
	}

	return false
}

func NewLocation(lat Latitude, lng Longitude, alt Altitude) Location {
	return Location{
		Latitude:  lat,
		Longitude: lng,
		Altitude:  alt,
	}
}

func ParseLocation(s string) (Location, error) {
	parts := strings.Split(s, ",")
	if len(parts) != 3 {
		return Location{}, fmt.Errorf("Not valid location string")
	}
	lat, err := ParseLatitude(parts[0])
	if err != nil {
		return Location{}, err
	}

	lng, err := ParseLongitude(parts[1])
	if err != nil {
		return Location{}, err
	}

	alt, err := ParseAltitude(parts[2])
	if err != nil {
		return Location{}, err
	}

	return NewLocation(lat, lng, alt), nil
}

func (l Location) Snap() Location {
	return NewLocation(
		l.Latitude.Snap(),
		l.Longitude.Snap(),
		l.Altitude.Snap(),
	)
}

// Calculates the distance between two points
func (l Location) Distance(loc Location) Measurement {
	alt1 := l.Altitude.ConvertAMSL()
	alt2 := loc.Altitude.ConvertAMSL()
	alt2.Measurement = alt2.Measurement.Convert(alt1.Measurement.Unit)

	mm := NewMeasurement(1, alt1.Measurement.Unit)
	latRatio1 := l.Latitude.ConverstionRatio(mm, alt1)
	lngRatio1 := l.Longitude.ConverstionRatio(mm, alt1)
	latRatio2 := loc.Latitude.ConverstionRatio(mm, alt2)
	lngRatio2 := loc.Longitude.ConverstionRatio(mm, alt2)

	latDiff := math.Abs(
		(l.Latitude.Float64() / latRatio1) - (loc.Latitude.Float64() / latRatio2),
	)
	lngDiff := math.Abs(
		(l.Longitude.Float64() / lngRatio1) - (loc.Longitude.Float64() / lngRatio2),
	)
	altDiff := math.Abs(alt1.Measurement.Amount - alt2.Measurement.Amount)

	amount := math.Sqrt(math.Pow(latDiff, 2) + math.Pow(lngDiff, 2) + math.Pow(altDiff, 2))
	return NewMeasurement(
		amount,
		alt1.Measurement.Unit,
	)
}

func (l Location) Equals(loc Location) bool {
	return l.Latitude == loc.Latitude && l.Longitude == loc.Longitude && l.Altitude.Equals(loc.Altitude)
}

func (l Location) String() string {
	return fmt.Sprintf("%g,%g,%s", l.Latitude, l.Longitude, l.Altitude.String())
}
