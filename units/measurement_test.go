package units

import (
	. "gopkg.in/check.v1"
)

type MeasurementSuite struct{}

var _ = Suite(&MeasurementSuite{})

func (s *MeasurementSuite) TestNew(c *C) {
	mm := NewMeasurement(9, Feet)
	c.Check(mm.Amount, Equals, 9.0)
	c.Check(mm.Unit, Equals, Feet)
}

func (s *MeasurementSuite) TestMath(c *C) {

	mm := NewMeasurement(9, Feet)

	// Addition
	mm = mm.Add(NewMeasurement(1, Feet))
	c.Check(mm.Amount, Equals, 10.0)

	// Subtraction
	mm = mm.Subtract(NewMeasurement(1, Feet))
	c.Check(mm.Amount, Equals, 9.0)

	// Multiply
	mm = mm.Multiply(NewMeasurement(2, Feet))
	c.Check(mm.Amount, Equals, 18.0)

	// Divide
	mm = mm.Divide(NewMeasurement(3, Feet))
	c.Check(mm.Amount, Equals, 6.0)
}

func (s *MeasurementSuite) TestConvert(c *C) {
	mm := NewMeasurement(12, Meters).Convert(Meters)
	c.Check(mm.Amount, Equals, 12.0)
	c.Check(mm.Unit, Equals, Meters)

	mm = NewMeasurement(12, Meters).Convert(Feet)
	c.Check(mm.Amount, Equals, 39.37008)
	c.Check(mm.Unit, Equals, Feet)

	mm = NewMeasurement(12, Feet).Convert(Meters)
	c.Check(mm.Amount, Equals, 3.657599882956804)
	c.Check(mm.Unit, Equals, Meters)
}

func (s *MeasurementSuite) TestEquals(c *C) {
	b := NewMeasurement(4, Feet).Equals(NewMeasurement(6, Feet))
	c.Check(b, Equals, false)
	b = NewMeasurement(4, Feet).Equals(NewMeasurement(4, Feet))
	c.Check(b, Equals, true)
	b = NewMeasurement(12, Feet).Equals(NewMeasurement(3.657599882956804, Meters))
	c.Check(b, Equals, true)
}

func (s *MeasurementSuite) TestString(c *C) {
	mm := NewMeasurement(9, Feet)
	c.Check(mm.String(), Equals, "9'")
}

func (s *MeasurementSuite) TestParse(c *C) {
	mm, err := ParseMeasurement("75m")
	c.Assert(err, IsNil)
	c.Check(mm.Equals(NewMeasurement(75, Meters)), Equals, true)

	mm, err = ParseMeasurement("23.287999414784018m")
	c.Assert(err, IsNil)
	c.Check(mm.Equals(NewMeasurement(23.287999414784018, Meters)), Equals, true)
}
