package units

import (
	"fmt"
	"math"
	"strconv"
	"strings"
)

type Unit string

var (
	Meters Unit = "m"    // meters
	Feet   Unit = "'"    // feet
	Lat    Unit = "lat"  // Latitude
	Long   Unit = "long" // Longitude
)

func StringToUnit(s string) Unit {
	switch strings.ToLower(s) {
	case "m", "meter", "meters":
		return Meters
	case "'", "ft", "foot", "feet":
		return Feet
	case "longitude", "long":
		return Long
	case "latitude", "lat":
		return Lat
	default:
		panic(fmt.Errorf("Unsupported unit: %s", s))
	}
}

func minMax(array []float64) (float64, float64) {
	var max float64 = array[0]
	var min float64 = array[0]
	for _, value := range array {
		if max < value {
			max = value
		}
		if min > value {
			min = value
		}
	}
	return min, max
}

func roundDownDecimalPoints(f float64, p int) float64 {
	pow := math.Pow(10, float64(p))
	multiplied := f * pow

	// since 100 * 1.13 == 112.99999999, we need to cast into a string and
	// back... Weird
	// https://play.golang.org/p/7OSyWFa1kpX
	str := fmt.Sprintf("%f", multiplied)
	multiplied, _ = strconv.ParseFloat(str, 64)

	return math.Floor(multiplied) / pow
}

func average(xs []float64) float64 {
	total := 0.0
	for _, v := range xs {
		total += v
	}
	return total / float64(len(xs))
}
