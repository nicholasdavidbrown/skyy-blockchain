package units

import (
	"fmt"
	"strings"
)

var AltSeg Measurement = Measurement{
	Amount: 10,
	Unit:   Meters,
}

var AltSegHalf Measurement = Measurement{
	Amount: AltSeg.Amount / 2,
	Unit:   AltSeg.Unit,
}

type Altitude struct {
	Measurement Measurement `json:"measurement"`
	Level       Level       `json:"level"`
}

type Altitudes []Altitude

var Ground = NewAltitude(
	NewMeasurement(0, Feet),
	AMSL,
)

func NewAltitude(mm Measurement, level Level) Altitude {
	return Altitude{
		Measurement: mm,
		Level:       level,
	}
}

func ParseAltitude(s string) (Altitude, error) {
	parts := strings.Split(s, " ")
	if len(parts) > 2 {
		return Altitude{}, fmt.Errorf("Not valid altitude input")
	}

	mm, err := ParseMeasurement(parts[0])
	if err != nil {
		return Altitude{}, err
	}
	lvl := AMSL
	if len(parts) > 1 {
		lvl = StringToLevel(parts[1])
	}
	return NewAltitude(mm, lvl), nil
}

func (a Altitude) ConvertAMSL() Altitude {
	if a.Level == AGL {
		// TODO: add support for conversion
		panic(fmt.Errorf("Above ground level not supported"))
	}
	return a
}

// Snap to nearest the altitude
func (a Altitude) Snap() Altitude {
	return a.SnapAndStep(0)
}

// Snap to nearest the altitude, +1
func (a Altitude) SnapUp() Altitude {
	return a.SnapAndStep(1)
}

// Snap to nearest the altitude, -1
func (a Altitude) SnapDown() Altitude {
	return a.SnapAndStep(-1)
}

func (a Altitude) SnapAndStep(incr int) Altitude {
	amsl := a.ConvertAMSL()
	amsl.Measurement = amsl.Measurement.Convert(Meters)

	amsl.Measurement = amsl.Measurement.Divide(AltSeg)
	amsl.Measurement.Amount = float64(int(amsl.Measurement.Amount))
	mm := AltSegHalf.Add(AltSeg.Multiply(amsl.Measurement))
	mm = mm.Add(AltSeg.Multiply(NewMeasurement(float64(incr), a.Measurement.Unit)))
	return NewAltitude(
		mm,
		amsl.Level,
	)
}

// Round down to the nearest class altitude
func (a Altitude) ClassAltitude() Altitude {
	// TODO: make a calculation instead of always choose 0 altitude.
	return NewAltitude(
		NewMeasurement(0, Feet),
		AMSL,
	)
}

func (a Altitude) Equals(aa Altitude) bool {
	return a.ConvertAMSL().Measurement.Equals(aa.ConvertAMSL().Measurement)
}

func (a Altitude) GreaterThan(aa Altitude) bool {
	return a.ConvertAMSL().Measurement.GreaterThan(aa.ConvertAMSL().Measurement)
}

func (a Altitude) GreaterThanOrEqualTo(aa Altitude) bool {
	return a.ConvertAMSL().Measurement.GreaterThanOrEqualTo(aa.ConvertAMSL().Measurement)
}

func (a Altitude) LessThan(aa Altitude) bool {
	return a.ConvertAMSL().Measurement.LessThan(aa.ConvertAMSL().Measurement)
}

func (a Altitude) LessThanOrEqualTo(aa Altitude) bool {
	return a.ConvertAMSL().Measurement.LessThanOrEqualTo(aa.ConvertAMSL().Measurement)
}

func (a Altitude) String() string {
	return fmt.Sprintf("%s %s", a.Measurement.String(), a.Level)
}

func (a Altitudes) MinMax() (Altitude, Altitude) {
	floats := make([]float64, len(a))
	for i, v := range a {
		floats[i] = v.ConvertAMSL().Measurement.Convert(Feet).Amount
	}

	minf, maxf := minMax(floats)
	min := NewAltitude(
		NewMeasurement(minf, Feet), AMSL,
	)
	max := NewAltitude(
		NewMeasurement(maxf, Feet), AMSL,
	)

	return min, max
}

func RangeAltitudes(input Altitudes) (altitudes Altitudes) {
	low, high := input.MinMax()
	low = low.ConvertAMSL().Snap()
	high = high.ConvertAMSL().Snap()

	current := low
	for current.LessThanOrEqualTo(high) {
		altitudes = append(altitudes, current)
		current = current.SnapUp()
	}

	return altitudes
}
