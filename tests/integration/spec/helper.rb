require 'net/http'
require 'pp'
require 'json'
require 'securerandom'
# require 'tempfile'

HOST = ENV['APIHOST'] || "localhost"
PORT = ENV['APIPORT'] || 1317
HTTP = Net::HTTP.new(HOST, PORT)

def get(path)
  resp = Net::HTTP.get_response(HOST, "/skyy#{path}", PORT)
  resp.body = JSON.parse(resp.body)
  return resp
end

def get_rand(len)
  str = SecureRandom.hex(len)
  return str.slice(0, len)
end

def makeTx(memo:'', hash:nil, sender:nil, coins:nil)
  hash ||= txid()
  sender ||= bnbAddress
  coins ||= [{
    'denom': 'RUNE-B1A',
    'amount': '1',
  }]
  return {
    'tx': hash,
    'sender': sender,
    'MEMO': memo,
    'coins': coins
  }
end

def reserveCylinder(start_time, end_time, loc, radius, height, user="jack", mode='block')
  request = Net::HTTP::Post.new("/skyy/cylinder")
  address = `skyycli keys show #{user} -a`.strip!
  request.body = {
    'start_time': start_time.iso8601,
    'expire_time': end_time.iso8601,
    'location': loc,
    'radius': radius,
    'height': height,
    'base_req': {
      'chain_id': "skyyNetwork",
      'gas': "auto",
      # gas_adjustment': "100",
      'from': address
    }
  }.to_json

  resp = HTTP.request(request)
  if resp.code != "200" 
    pp resp.body
    return resp
  end

  resp.body = JSON.parse(resp.body)
  # resp.body.values[1]['fee']['gas'] = "200146"
  signedJson = sign(resp.body.to_json, user, mode)
  # pp signedJson

  return broadcast(signedJson)
end


def reserveSpace(start_time, end_time, triangles, user="jack", mode='block')
  request = Net::HTTP::Post.new("/skyy/polyhedron")
  address = `skyycli keys show #{user} -a`.strip!
  request.body = {
    'start_time': start_time.iso8601,
    'expire_time': end_time.iso8601,
    'triangles': triangles,
    'base_req': {
      'chain_id': "skyyNetwork",
      'gas': "auto",
      # gas_adjustment': "100",
      'from': address
    }
  }.to_json

  resp = HTTP.request(request)
  if resp.code != "200" 
    pp resp.body
    return resp
  end

  resp.body = JSON.parse(resp.body)
  # resp.body.values[1]['fee']['gas'] = "200146"
  signedJson = sign(resp.body.to_json, user, mode)
  # pp signedJson

  return broadcast(signedJson)
end

def sign(tx, user="jack", mode="block")
  File.open("/tmp/unSigned.json", "w") { |file| file.puts tx}
  signedTx = `echo "password" | skyycli tx sign /tmp/unSigned.json --from #{user}`
  signedTx = JSON.parse(signedTx)
  return {
    'mode': mode,
    'tx': signedTx['value'],
  }
end

def broadcast(tx) 
  request = Net::HTTP::Post.new("/txs")
  request.body = tx.to_json
  resp = HTTP.request(request)
  resp.body = JSON.parse(resp.body)
  return resp
end
