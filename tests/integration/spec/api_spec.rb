require_relative './helper.rb'
require 'time'

describe "API Tests" do

  context "Check healthcheck responds" do
    it "should return 'pong'" do
      resp = get("/ping")
      expect(resp.code).to eq("200")
      expect(resp.body['ping']).to eq "pong"
    end
  end

  context "Reserve airspace" do
    triangles = [
      %w{42,-72,0'\ AMSL 42,-71,0'\ AMSL 43,-72,100'\ AMSL},
      %w{42,-71,0'\ AMSL 43,-72,100'\ AMSL 43,-71,0'\ AMSL},
      %w{43,-72,100'\ AMSL 43,-71,0'\ AMSL 42,-72,0'\ AMSL},
      %w{42,-72,0'\ AMSL 43,-71,0'\ AMSL 42,-71,0'\ AMSL},
    ]

    it "should be able to reserve airspace" do
      start_time = Time.now.utc
      end_time = start_time + 60*60
      resp = reserveSpace(start_time, end_time, triangles)
      expect(resp.code).to eq("200"), resp.body.inspect
      expect(resp.body['logs'][0]['success']).to eq(true), resp.body.to_json
    end

    it "should be able to reserve cylinder airspace" do
      start_time = Time.now.utc
      end_time = start_time + 60*60
      resp = reserveCylinder(start_time, end_time, "42,-70,0' AMSL", "1000m", "500m")
      expect(resp.code).to eq("200"), resp.body.inspect
      expect(resp.body['logs'][0]['success']).to eq(true), resp.body.to_json
    end

    it "should be able to retrieve reserved airspace" do
      resp = get("/sector?point=90,180,0'%20ASML&point=-90,-180,0'%20ASML&ts=#{Time.now.utc.strftime('%FT%TZ')}")
      expect(resp.code).to eq("200"), resp.body.inspect
      expect(resp.body['result'].length).to eq(2), resp.body.to_json
    end

    it "should be able to reserve overlapping polygon but at different time" do
      start_time = Time.now.utc + 100*60
      end_time = start_time + 60*60
      resp = reserveSpace(start_time, end_time, triangles)
      expect(resp.code).to eq("200"), resp.body.inspect
      expect(resp.body['logs'][0]['success']).to eq(true), resp.body.to_json
    end

    it "should not be able to reserve same polyhedron again" do
      start_time = Time.now.utc
      end_time = start_time + 60*60
      resp = reserveSpace(start_time, end_time, triangles)
      expect(resp.body['logs'][0]['success']).to eq(false), resp.body.to_json
    end

    skip "should not be able to reserve overlapping polyhedron" do
      triangles = [
        %w{0.5,0.5,0.5'\ AMSL 0.5,1.5,0.5'\ AMSL 1.5,0.5,0.5'\ AMSL},
        %w{0.5,1.5,0.5'\ AMSL 1.5,0.5,0.5'\ AMSL 1.5,1.5,0.5'\ AMSL},
        %w{1.5,0.5,0.5'\ AMSL 1.5,1.5,0.5'\ AMSL 0.5,0.5,0.5'\ AMSL},
        %w{0.5,0.5,0.5'\ AMSL 1.5,1.5,0.5'\ AMSL 0.5,1.5,0.5'\ AMSL},
      ]
      start_time = Time.now.utc
      end_time = start_time + 60*60
      resp = reserveSpace(start_time, end_time, triangles)
      expect(resp.body['logs'][0]['success']).to eq(false), resp.body.to_json
    end

  end


end
