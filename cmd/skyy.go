package cmd

const Bech32PrefixAccAddr = "skyy"
const Bech32PrefixAccPub = "skyypub"
const Bech32PrefixValAddr = "skyyv"
const Bech32PrefixValPub = "skyyvpub"
const Bech32PrefixConsAddr = "skyyc"
const Bech32PrefixConsPub = "skyycpub"
