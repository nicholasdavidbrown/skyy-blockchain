require 'xmlsimple'
require 'ruby-progressbar'


puts "Starting..."
puts "Reading file..."
starting = Time.now
xml = File.read 'airports.small.xml'
puts "Done. (#{Time.now - starting} seconds)"
puts "Parsing file..."
starting = Time.now
data = XmlSimple.xml_in xml
puts "Done. (#{Time.now - starting} seconds)"

`rm /tmp/airports.log`

i = 1
ilimit = 100
puts "Creating airports...."

progressbar = ProgressBar.create(
  :title => "Airports", 
  :total => data["AIRPORT"].length,
  :format => "%a %e %P% Processed: %c from %C",
)

for airport in data["AIRPORT"]
  mode = "async"
  if i == ilimit then
    mode = "async"
  end
  cmd = "echo 'password' | skyycli tx skyy add-cylinder --broadcast-mode=#{mode} --gas auto --from batch#{i} -y -- \"#{airport["AptRefPoint_DD"][0]["LatitudeDD"][0]},#{airport["AptRefPoint_DD"][0]["LongitudeDD"][0]},0m AMSL\" 5000m 100000m permissioned >> /tmp/airports.log 2> /dev/null"
  # puts cmd
  `#{cmd}`
  if i == ilimit then 
    i = 1
  else 
    i += 1
  end
  progressbar.increment
end
