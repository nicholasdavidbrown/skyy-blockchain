Data for the Skyy Project in XML format

To upload all airport data into skyy network....

```bash
# create 100 users
# TODO remove this requirement
bash create_bash.sh

make -C .. reset start-daemon
```

```bash
# install ruby 2.6.3
bundle install
ruby airport.rb
```

There are two airport xml files. One is small to quick upload, and the other
is 10,000's which will take many hours to upload. For dev purposes, use the
smaller file.
