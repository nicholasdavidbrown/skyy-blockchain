include Makefile.ledger
all: lint install

install: go.sum
		go install -mod=readonly $(BUILD_FLAGS) ./cmd/skyycli
		go install -mod=readonly $(BUILD_FLAGS) ./cmd/skyyd

go.sum: go.mod
		@echo "--> Ensure dependencies have not been modified"
		GO111MODULE=on go mod verify

lint:
	@golangci-lint run --deadline=15m
	@go mod verify

test:
	@go test -mod=readonly ./...

clear: 
	clear

test-watch: clear
	@./scripts/watch.bash

build:
	@go build ./...

start: install start-daemon

start-daemon:
	skyyd start

start-rest:
	skyycli rest-server

start-cors:
	yarn --cwd scripts/cors start

clean:
	rm -rf ~/.skyyd
	skyyd unsafe-reset-all

setup:
	./scripts/setup.sh

reset: clean
	./scripts/reset.bash

export:
	@skyyd export
